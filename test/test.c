#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

typedef struct SetupSdl SetupSdl;
struct SetupSdl{
  SDL_Surface *screen;
  TTF_Font *police;
  SDL_Color color;
  int endOfProg;
  SDL_Event event;
  SDL_Surface *fullScreen;
  SDL_Rect position;
};

void displayText(char *text, SetupSdl sdlConf, int x, int y);
void inputData(char **text, SetupSdl *sdlConf, int x, int y, int *sizeOfInput);


int main(int argc, char *argv[])
{
  SetupSdl dataSdl;
  int x = 60, y = 370;
  dataSdl.endOfProg = 1;
  SDL_Init(SDL_INIT_VIDEO);
  TTF_Init();

  dataSdl.screen = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
  SDL_WM_SetCaption("Gestion du texte avec SDL_ttf", NULL);
  dataSdl.fullScreen = SDL_LoadBMP("test.bmp");
  dataSdl.police = TTF_OpenFont("calibri.ttf", 30);
  dataSdl.position.x = 0;
  dataSdl.position.y = 0;
  int sizeOfInput = 25;
  char * text = malloc(sizeOfInput);
  char * test;
  text[0] = '\0';
  SDL_BlitSurface(dataSdl.fullScreen, NULL, dataSdl.screen, &dataSdl.position);
  SDL_Flip(dataSdl.screen);

  while (dataSdl.endOfProg)
  {
    SDL_WaitEvent(&dataSdl.event);
    inputData(&text, &dataSdl, 200, 400, &sizeOfInput);
    switch(dataSdl.event.type)
    {
      case SDL_QUIT:
      dataSdl.endOfProg = 0;
      break;
    }
  }
  SDL_Quit();

  return EXIT_SUCCESS;
}

void inputData(char **text, SetupSdl *sdlConf, int x, int y, int *sizeOfInput){
  char inputKey;
  int currentIndex;

  switch(sdlConf->event.type)
  {
    case SDL_KEYDOWN:
      currentIndex = strlen(*text);
      inputKey = sdlConf->event.key.keysym.sym;
      if((inputKey > 64 && inputKey < 91) || (inputKey > 96 && inputKey < 123)){
        if(currentIndex < *sizeOfInput - 1){
          (*text)[currentIndex] = inputKey;
          (*text)[currentIndex + 1] = '\0';
        }
      }
      else if(inputKey == 8) (*text)[--currentIndex] = '\0';
      else if(inputKey == 13) return;
      SDL_BlitSurface(sdlConf->fullScreen, NULL, sdlConf->screen, &sdlConf->position);
      SDL_Flip(sdlConf->screen);
      displayText(*text, *sdlConf, x, y);
      break;
  }

}


void displayText(char *text, SetupSdl sdlConf, int x, int y){
  SDL_Surface *textSurface = TTF_RenderText_Blended(sdlConf.police, text, sdlConf.color);
  SDL_Rect position;
  position.x = x;//60;
  position.y = y; //370;
  SDL_BlitSurface(textSurface, NULL, sdlConf.screen, &position);
  SDL_Flip(sdlConf.screen);
  SDL_FreeSurface(textSurface);
}
