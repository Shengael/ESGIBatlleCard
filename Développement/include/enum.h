/**
 * @Author: Luis VALDEZ
 * @Date:   2017-12-07T21:25:52+01:00
 * @Email:  valdez.jds.16@gmail.com
 * @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-17T00:37:42+01:00
 * @Copyright: Shengael
 */

 #define AGGRESSIVE 1
 #define RESEARCH 2
 #define DEFENSIVE 3
 #define ZOMBI 4
 //spellEffect
 #define ADDLIFE 1
 #define ADDATTACK 2
 #define DRAWCARD 3
 #define ATTACK 4
 //event
 #define BOARDSHUFFLE 0
 #define HANDSHUFFLE 1
 #define DECKSHUFFLE 2


#ifndef ENUM_H_INCLUDED
#define ENUM_H_INCLUDED
//specialEffect


//boolean
typedef enum bool bool;
enum bool{false, true};


//variable globale


#endif
