#ifndef GAMEEVENT_H_INCLUDED
#define GAMEEVENT_H_INCLUDED


void eventInGame(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);

void gameplayEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void playCardInit(SetupSdl *sdlConf, playerData *player);
void playCardEnd(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard);
void attackInit(SetupSdl *sdlConf, playerData *player);
void attackEnd(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard);

void globalEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void PLAYServantEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard);
void playResearch(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard);
void playSpellEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard);



#endif
