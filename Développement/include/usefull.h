/**
 * @Author: Luis VALDEZ
 * @Date:   2017-12-11T19:44:00+01:00
 * @Email:  valdez.jds.16@gmail.com
 * @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-11T19:55:07+01:00
 * @Copyright: Shengael
 */

#ifndef USEFULL_H_INCLUDED
#define USEFULL_H_INCLUDED

void freeThreeDimensionArrayChar(char ****array, int line, int column);



#endif
