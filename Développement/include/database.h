/**
 * @Author: Luis VALDEZ
 * @Date:   2017-12-07T21:58:49+01:00
 * @Email:  valdez.jds.16@gmail.com
 * @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-10T01:02:19+01:00
 * @Copyright: Shengael
 */



#ifndef DATABASE_H_INCLUDED
#define DATABASE_H_INCLUDED

MYSQL *connectDb();
void dataPull(MYSQL *mysql, char *query, char ****string, int *line, int *column);
char ***addLineResult(char ****request, int numChamps, int line);

#endif
