/**
* @Author: Luis VALDEZ
* @Date:   2017-12-09T13:44:56+01:00
* @Email:  valdez.jds.16@gmail.com
* @Project: ESGIBATTLECARD
* @Last modified by:   Luis VALDEZ
* @Last modified time: 2017-12-18T12:08:16+01:00
* @Copyright: Shengael
*/



#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED
#include <enum.h>

typedef struct playerData playerData;
struct playerData{
  int eventStatus;
  int idPlayer;
  char *pseudo;
  char *passWord;
  char *vPassword;
  int idGame;
  int lastPlay;
  int currentIndex;
  int endTurn;
  int endGame;
  int open;
  int win;
};

int searchGame(MYSQL *mysql, int idPlayer);
bool playerInGame(MYSQL *mysql, int idPlayer);
int createGame(MYSQL *mysql, int idSearch, int idTournament);
int *GetPlayers(MYSQL *mysql, int idSearch);
int createGameTable(MYSQL *mysql, int firstPlayer, int playerStudent, int idTournament);
void createDeckTable(MYSQL *mysql, int *players, int idGame);
void createHandTable(MYSQL *mysql, int *players, int idGame);
void createPlayTable(MYSQL *mysql, int *players, int idGame);
int getIdGame(MYSQL *mysql, int idPlayer);


int isStudent(MYSQL *mysql, int idPlayer, int idGame);
int gameRoundInit(MYSQL *mysql, int idGame, int idPlayer);
int changeTurn(MYSQL *mysql, int idGame, int idPlayer);
int itIsMyTurn(MYSQL *mysql, int idGame, int idPlayer);
int incRound(MYSQL *mysql, int idGame, int idPlayer);
int itIsTheFirst(MYSQL *mysql, int idGame, int idPlayer);
void updateOpenRemaining(MYSQL *mysql, int openRemaining, int idGame, int idPlayer);


bool drawCard(MYSQL *mysql, int idGame, int idPlayer);
bool playCard(MYSQL *mysql, int idGame, int idPlayer, int IndexCard, int idTarget, int *open);
bool playServant(MYSQL *mysql, int idGame, int idPlayer, int idCard);

int spellCat(MYSQL *mysql, int idCard);
int spellValue(MYSQL *mysql, int idCard);
bool playSpell(MYSQL *mysql, int idGame, int idPlayer, int idCard, int idTarget);
int addAttackSpell(MYSQL *mysql, int idGame,int idCard, int idTarget);

int returnIdCard(MYSQL *mysql, int IndexCard, char *localisation, int idGame, int idPlayer);
bool emptySlot(MYSQL *mysql, int idGame, int idPlayer, char *location);
bool existTarget(MYSQL *mysql, int idGame);
bool isServant(MYSQL *mysql, int idCard);
bool enoughOpen(MYSQL *mysql, int *openRemaining, int idCard, int idGame, int idPlayer);

bool attack(MYSQL *mysql, int idCardAttacker, int idCardTarget, int idGame, int idPlayer);
bool death(MYSQL *mysql, int idGame, char *element, int IdElement);
bool addValue(MYSQL *mysql, int idGame, int value, int idTarget, char *target);
void getCardValue(MYSQL *mysql, int idGame, int idCard, int *life, int *attack);
int getIdEnemy(MYSQL *mysql, int idGame, int idPlayer);
int attackEnemy(MYSQL *mysql, int idGame, int idEnemy, int idCard);
int attackEnemyCard(MYSQL *mysql, int idCardAttacker, int idGame, int cardTarget);
bool isSpecialCard(MYSQL *mysql, int idCard, int specialEffect);
bool otherCardAreSpecial(MYSQL *mysql, int idCard, int specialEffect, int player, int idGame);


bool isNotSleep(MYSQL *mysql, int idCard, int idGame, int idPlayer);
void unSleep(MYSQL *mysql, int idGame, int idPlayer);
void sleepCard(MYSQL *mysql, int idCard, int idGame);

int endGame(MYSQL *mysql, int idGame, int idPlayer);
int countCard(MYSQL *mysql, int idGame, int idPlayer);
int getPlayerLife(MYSQL *mysql, int idGame, int idPlayer);
void finishGame(MYSQL *mysql, int idGame, int loser);
void incDataPlayer(MYSQL *mysql, int player, char *data);
int gameIsEnd(MYSQL *mysql, int idGame);

#endif
