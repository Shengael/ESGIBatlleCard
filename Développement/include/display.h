/**
 * @Author: Luis VALDEZ
 * @Date:   2017-12-17T00:19:29+01:00
 * @Email:  valdez.jds.16@gmail.com
 * @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-18T12:05:32+01:00
 * @Copyright: Shengael
 */
#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

#include <game.h>
#include <SDL/SDL.h>
#include <SDL/sdl_ttf.h>

typedef struct SetupSdl SetupSdl;
struct SetupSdl{
  SDL_Surface *screen;
  TTF_Font *police;
  TTF_Font *policeText;
  TTF_Font *policeDeck;
  SDL_Color color;
  SDL_Color error;
  int focusWrite;
  int endDisplay;
  SDL_Event event;
  SDL_Surface *fullScreen;
  SDL_Rect position;
  int xHand[16];
  int yHand[2];
  int xHandEnemy[16];
  int yHandEnemy[2];
  int xBoard[12];
  int yBoard[2];
  int xBoardEnemy[12];
  int yBoardEnemy[2];
  double xResize;
  double yResize;
  char pathOfPicture[30];
  char pathOfTiny[30];
  char errorText[35];
};
SetupSdl *initDisplay();

//gestion evenement
void eventManagement(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void eventDisplay(SetupSdl *sdlConf, playerData *player);
void eventSearch(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void eventEndGame(playerData *player, SetupSdl *sdlConf);
void connectionEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void initGame(SetupSdl *sdlConf, playerData *player);
void registrationEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void indexEvent(SetupSdl *sdlConf, playerData *player);

//affichage
void displayBoard(SetupSdl *sdlConf, int deck);
void displayLoading(SetupSdl *sdlConf);
void displaySearch(SetupSdl *sdlConf);
void displayFullScreen(SetupSdl *sdlConf, char *pathImage);
void displayCard(SetupSdl *sdlConf, char *pathImage, int x, int y, int size);
void updateBoard(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void displayData(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard, int x, int y);
void displayOpen(MYSQL *mysql, SetupSdl *sdlConf, playerData *player);
void displayEndGame(SetupSdl *sdlConf, int win);

int checkRect(SetupSdl *sdlConf, int xMin, int xMax, int yMin, int Ymax);

#endif
