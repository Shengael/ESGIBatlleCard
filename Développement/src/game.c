/**
* @Author: Luis VALDEZ <Shengael>
* @Date:   2017-12-09T13:44:48+01:00
* @Email:  valdez.jds.16@gmail.com
* @Project: ESGIBATTLECARD
* @Last modified by:   Luis VALDEZ
* @Last modified time: 2017-12-19T23:27:48+01:00
* @Copyright: Shengael
*/


#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <errno.h>
#include <winsock.h>
#include <time.h>
#include "MYSQL/mysql.h"
#include "database.h"
#include "enum.h"
#include "game.h"
#include "usefull.h"




int searchGame(MYSQL *mysql, int idPlayer){
  int numberRow, numberData, idGame, checkModify;
  char ***data = NULL, query[1000];
  if(playerInGame(mysql, idPlayer)) return -1;
  sprintf(query,"SELECT idSearch FROM SEARCH_GAME WHERE idPlayer2 IS NULL ORDER BY rand() LIMIT 1");
  dataPull(mysql, query , &data, &numberRow, &numberData);
  fflush(stdout);
  if(!numberRow){
    freeThreeDimensionArrayChar(&data, numberRow, numberData);
    sprintf(query,"INSERT INTO SEARCH_GAME(idPlayer) VALUE(%d)",idPlayer);
    mysql_query(mysql, query);
    while(!playerInGame(mysql, idPlayer)){
      Sleep(5000);
    }
    return getIdGame(mysql, idPlayer);
  }
  else{
    sprintf(query,"UPDATE SEARCH_GAME SET idPlayer2 = %d WHERE idSearch = %d AND idPlayer2 IS NULL", idPlayer, atoi(data[0][0]));
    mysql_query(mysql, query);
    checkModify = (int) mysql_affected_rows(mysql);
    if(!checkModify) searchGame(mysql, idPlayer);
    else{
      idGame = createGame(mysql, atoi(data[0][0]), 0);
    }
  }

  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  return idGame;
}

int getIdGame(MYSQL *mysql, int idPlayer){
  char ***data = NULL, query[1000];
  int numberRow, numberData, idGame;

  sprintf(query,"SELECT matchId FROM PLAY WHERE idPlayer = %d", idPlayer);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data == NULL) return -1;
  idGame = atoi(data[0][0]);

  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  return idGame;
}


bool playerInGame(MYSQL *mysql, int idPlayer){
  int numberRow, numberData;
  char ***data = NULL, query[1000];
  sprintf(query,"SELECT 1 FROM PLAY WHERE idPlayer = %d", idPlayer);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data != NULL){
    freeThreeDimensionArrayChar(&data, numberRow, numberData);
    return true;
  }
  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  return false;
}


/**
* Création de la partie
* @param  mysql       connexion MYSQL
* @param  idPlayerOne l'id de l'utilisateur numero 1
* @param  idPlayerTwo l'id de l'utilisateur numero 2
* @return             retourne l'id de la partie
*/
int createGame(MYSQL *mysql, int idSearch, int idTournament){
  srand(time(NULL));
  int numberRow, numberData, firstPlayer = rand() % 2, idGame, *players, deck = rand() % 2, playerDeck[2];
  char ***data = NULL, query[1000];
  players = GetPlayers(mysql, idSearch);

  playerDeck[0] = players[deck];
  playerDeck[1] = players[1 - deck];
  firstPlayer = players[firstPlayer];
  idGame = createGameTable(mysql, firstPlayer, playerDeck[0], idTournament);
  createDeckTable(mysql, playerDeck, idGame);
  createHandTable(mysql, players, idGame);
  createPlayTable(mysql, players, idGame);
  free(players);
  return idGame;
}

int *GetPlayers(MYSQL *mysql, int idSearch){
  int numberRow, numberData, *players = malloc(sizeof(int) * 2);
  char ***data = NULL, query[1000];
  sprintf(query,"SELECT idPlayer, idPlayer2 FROM SEARCH_GAME where idSearch = %d", idSearch);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  players[0] =  atoi(data[0][0]);
  players[1] =  atoi(data[0][1]);
  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  sprintf(query,"DELETE FROM SEARCH_GAME WHERE idSearch = %d", idSearch);
  mysql_query(mysql, query);
  return players;
}


int createGameTable(MYSQL *mysql, int firstPlayer, int playerStudent, int idTournament){
  int numberRow, numberData, idGame;
  char ***data = NULL, query[1000];
  if(idTournament == 0){
    sprintf(query,"INSERT INTO GAME(turn, firstPlayer, student) VALUE(%d, %d, %d)", firstPlayer, firstPlayer, playerStudent);
    mysql_query(mysql, query);
  }
  else{
    sprintf(query,"INSERT INTO GAME(turn, firstPlayer, student, tournamentId) VALUE(%d, %d, %d)", firstPlayer, firstPlayer, idTournament);
    mysql_query(mysql, query);
  }

  sprintf(query,"SELECT matchId FROM GAME where turn = %d AND firstPlayer = %d", firstPlayer, firstPlayer);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  idGame = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  return idGame;
}


void createDeckTable(MYSQL *mysql, int *players, int idGame){
  int numberRow, numberData, i, y;
  char ***data = NULL, query[1000];

  for(i = 0; i < 2; i++){
    sprintf(query,"SELECT cardId FROM CARD WHERE deck = %d ORDER BY cardId", i);
    dataPull(mysql, query , &data, &numberRow, &numberData);

    for(y = 0; y < numberRow; y++){
      sprintf(query,"INSERT INTO DECK(cardId, matchId, idPlayer) VALUE(%d,%d,%d)", atoi(data[y][0]), idGame, players[i]);
      mysql_query(mysql, query);
    }
  }
  freeThreeDimensionArrayChar(&data, numberRow, numberData);
}



void createHandTable(MYSQL *mysql, int *players, int idGame){
  int i, y;
  for(i = 0; i < 2; i++){
    for(y = 0; y < 3; y++){
      printf("\nfunction drawCard init joueur %d pioche numéro %d\n", players[i], y);
      fflush(stdout);
      drawCard(mysql, idGame, players[i]);
    }
  }
}



void createPlayTable(MYSQL *mysql, int *players, int idGame){
  int i;
  char query[1000];
  for (i = 0; i < 2; i++) {
    sprintf(query,"INSERT INTO PLAY(idPlayer, matchId) VALUE(%d, %d)", players[i], idGame);
    mysql_query(mysql, query);
  }
}


int isStudent(MYSQL *mysql, int idPlayer, int idGame){
  int numberRow, numberData, student;
  char ***data = NULL, query[1000];

  sprintf(query,"SELECT student FROM GAME WHERE matchid = %d", idGame);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data == NULL) return -1;
  student = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberRow, numberData);

  if(idPlayer == student) return 1;
  return 0;

}

/**
* Effectue un tour dans la partie
* @param mysql    [description]
* @param idGame   [description]
* @param idPlayer [description]
* @return             retourne TRUE si tout s'est bien passé
*/
int gameRoundInit(MYSQL *mysql, int idGame, int idPlayer){
  int roundValue = 0;

  if(itIsMyTurn(mysql, idGame, idPlayer)){
    roundValue = incRound(mysql, idGame, idPlayer);
    unSleep(mysql, idGame, idPlayer);
    printf("drawGameroundInit\n");
    drawCard(mysql, idGame, idPlayer);
  }
  printf("game.c : ligne 207 roundValue %d\n", roundValue);
  fflush(stdout);
  return roundValue;
}

int changeTurn(MYSQL *mysql, int idGame, int idPlayer){
  int numberOfResult, numberOfData, *players, i;
  char ***data = NULL;
  char query[1000];
  sprintf(query,"UPDATE GAME SET turn = %d WHERE matchId = %d", getIdEnemy(mysql, idGame, idPlayer), idGame);
  mysql_query(mysql, query);
  printf("changeTurn\n");
  fflush(stdout);
  return 1;
}

void updateOpenRemaining(MYSQL *mysql, int openRemaining, int idGame, int idPlayer){
  char query[1000];
  sprintf(query,"UPDATE PLAY SET openRemaining = %d WHERE matchId = %d AND idPlayer = %d", openRemaining, idGame, idPlayer);
  mysql_query(mysql, query);
}


int itIsMyTurn(MYSQL *mysql, int idGame, int idPlayer){
  int numberOfResult, numberOfData, turnPlayer;
  char ***data = NULL;
  char query[1000];
  sprintf(query,"SELECT turn FROM GAME WHERE matchId = %d", idGame);
  dataPull(mysql, query , &data, &numberOfResult, &numberOfData);
  if(data == NULL) return -1;
  turnPlayer = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  if(turnPlayer == idPlayer) return 1;
  else return 0;
}

int incRound(MYSQL *mysql, int idGame, int idPlayer){
  int numberOfResult, numberOfData, roundValue;
  char ***data = NULL, query[1000];

  sprintf(query,"SELECT ROUND FROM GAME WHERE matchId = %d", idGame);
  dataPull(mysql, query , &data, &numberOfResult, &numberOfData);
  if(data == NULL) return -1;
  printf("round value : %sxx\n", data[0][0]);
  roundValue = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);

  if(itIsTheFirst(mysql, idGame, idPlayer)){
    roundValue++;
    printf("i'm the first round value %d\n", roundValue);
    sprintf(query,"UPDATE GAME SET round = %d WHERE matchId = %d", roundValue, idGame);
    mysql_query(mysql, query);
  }

  return roundValue;
}


int itIsTheFirst(MYSQL *mysql, int idGame, int idPlayer){
  int numberOfResult, numberOfData, firstPlayer;
  char ***data = NULL, query[1000];
  sprintf(query,"SELECT firstPlayer FROM GAME WHERE matchId = %d", idGame);
  dataPull(mysql, query , &data, &numberOfResult, &numberOfData);
  if(data == NULL) return -1;
  printf("firstplayer %s and idPlayer %d\n", data[0][0], idPlayer);
  firstPlayer = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfData, numberOfData);
  if(firstPlayer == idPlayer) return 1;
  else return 0;
}
/**
* Supprime une carte aléatoire du deck et l'ajoute dans la main
* @param mysql    connexion mysql
* @param idGame   l'id de la partie dans la bdd
* @param idPlayer l'id de l'utilisateur en cours dans la bdd
* @return             retourne TRUE si tout s'est bien passé
*/
bool drawCard(MYSQL *mysql, int idGame, int idPlayer){
  int numberOfCard, numberOfDataCard, idOfCardDraw;
  char ***data = NULL;
  char query[1000];
  sprintf(query,"SELECT cardId FROM DECK WHERE matchId = %d AND idPlayer = %d ORDER BY RAND() LIMIT 1", idGame, idPlayer);
  dataPull(mysql, query , &data, &numberOfCard, &numberOfDataCard);
  if(data == NULL) return false;
  idOfCardDraw = atoi(data[0][0]);
  sprintf(query,"DELETE FROM DECK WHERE cardId = %d AND matchId = %d AND idPlayer = %d", idOfCardDraw, idGame, idPlayer);
  mysql_query(mysql, query);

  if(emptySlot(mysql, idGame, idPlayer, "HAND")){
    sprintf(query,"INSERT INTO HAND (cardId, matchId , idPlayer) VALUES (%d, %d, %d)", idOfCardDraw, idGame, idPlayer);
    mysql_query(mysql, query);
  }
  freeThreeDimensionArrayChar(&data, numberOfCard, numberOfDataCard);
  return true;
}


/**
* Verifie s'il y a un emplacement disponible dans location'
* @param  mysql    connexion mysql
* @param  idGame   l'id de la partie dans la bdd
* @param  idPlayer l'id de l'utilisateur en cours dans la bdd
* @param  location HAND ou BOARD
* @return          retourne vrai si la main peut recevoir une nouvelle carte
*/
bool emptySlot(MYSQL *mysql, int idGame, int idPlayer, char *location){
  int numberOfCardInHand, numberOfDataCard;
  char ***data = NULL;
  char query[1000];
  sprintf(query, "SELECT 1 FROM %s WHERE idPlayer = %d AND matchId = %d", location,  idPlayer, idGame);
  dataPull(mysql,query , &data, &numberOfCardInHand, &numberOfDataCard);
  freeThreeDimensionArrayChar(&data, numberOfCardInHand, numberOfDataCard);

  if(!strcmp(location, "HAND") && numberOfCardInHand < 8) return true;
  if(!strcmp(location, "BOARD") && numberOfCardInHand < 6) return true;
  return false;
}

/**
* Supprime une carte chosie par l'utilisateur de la main et l'ajoute sur le board
* @param mysql    connexion mysql
* @param idGame   l'id de la partie dans la bdd
* @param idPlayer l'id de l'utilisateur en cours dans la bdd
* @return             retourne TRUE si tout s'est bien passé
*/

int returnIdCard(MYSQL *mysql, int IndexCard, char *localisation, int idGame, int idPlayer){
  int numberOfResult, numberOfData, idCard;
  char query[1000], ***data = NULL;

  sprintf(query,"SELECT cardId FROM %s WHERE matchId = %d AND idPlayer = %d ORDER BY cardId", localisation, idGame, idPlayer);
  dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
  printf("number : %d index : %d compare : %d\n", numberOfResult, IndexCard, IndexCard > numberOfResult - 1);
  if(IndexCard > numberOfResult - 1) return -1;
  idCard = atoi(data[IndexCard][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return idCard;
}


bool playCard(MYSQL *mysql, int idGame, int idPlayer, int idCard, int idTarget, int *open){
  if(!enoughOpen(mysql, open, idCard, idGame, idPlayer)) return false;
  printf("canPlayCard\n");
  if(isServant(mysql, idCard)) return playServant(mysql, idGame, idPlayer, idCard);
  else return playSpell(mysql, idGame, idPlayer, idCard, idTarget);
}


bool playServant(MYSQL *mysql, int idGame, int idPlayer, int idCard){
  char query[1000];

  if(emptySlot(mysql, idGame, idPlayer, "BOARD")){
    printf("empty slot\n");
    fflush(stdout);
    sprintf(query,"DELETE FROM HAND WHERE cardId = %d AND matchId = %d AND idPlayer = %d", idCard, idGame, idPlayer);
    mysql_query(mysql, query);
    sprintf(query,"INSERT INTO BOARD (cardId, matchId , idPlayer, sleep) VALUES (%d, %d, %d, 1)", idCard, idGame, idPlayer);
    mysql_query(mysql, query);
    if(isSpecialCard(mysql, idCard, AGGRESSIVE)){
      sprintf(query,"UPDATE BOARD SET sleep = 0 WHERE cardId = %d AND matchId = %d AND idPlayer = %d", idCard, idGame, idPlayer);
      mysql_query(mysql, query);
    }
    if(isSpecialCard(mysql, idCard, RESEARCH)){
      printf("\nfunction drawCard serviteur pioche\n");
      fflush(stdout);
      drawCard(mysql, idGame, idPlayer);
    }
    return true;
  }
  return false;
}

int spellCat(MYSQL *mysql, int idCard){
  int numberOfResult, numberOfData, spell;
  char query[1000], ***data = NULL;
  sprintf(query,"SELECT spell FROM CARD WHERE cardId = %d", idCard);
  dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
  if(dataPull == NULL) return -1;
  spell = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return spell;
}

int spellValue(MYSQL *mysql, int idCard){
  int numberOfResult, numberOfData, spellValueCard;
  char query[1000], ***data = NULL;
  sprintf(query,"SELECT spellEffect FROM CARD WHERE cardId = %d", idCard);
  dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
  if(dataPull == NULL) return -1;
  spellValueCard = atoi(data[0][0]);
  printf("spell value : %d\n", spellValueCard);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return spellValueCard;
}


bool playSpell(MYSQL *mysql, int idGame, int idPlayer, int idCard, int idTarget){
  int spell, value, i;
  char query[1000];

  spell = spellCat(mysql, idCard);
  value = spellValue(mysql, idCard);

  switch(spell){

    case ADDLIFE :
    printf("ADDLIFE value %d\n", value);
    fflush(stdout);
    addValue(mysql, idGame, value, idTarget, "lifeModified");
    break;

    case ADDATTACK :
    printf("ADDATTACK value %d\n", value);
    fflush(stdout);
    addValue(mysql, idGame, value, idTarget, "attackModified");
    break;

    case DRAWCARD :
    printf("DRAWCARD value %d\n", value);
    fflush(stdout);
    for(i = 0; i < value; i++){
      drawCard(mysql, idGame, idPlayer);
    }
    break;

    case ATTACK :
    printf("ATTACK value %d\n", value);
    fflush(stdout);
    addValue(mysql, idGame, -value, idTarget, "lifeModified");
    death(mysql, idGame, "BOARD", idTarget);
    break;
  }

  sprintf(query,"DELETE FROM HAND WHERE cardId = %d AND matchId = %d", idCard, idGame);
  mysql_query(mysql, query);
  return true;
}


bool death(MYSQL *mysql, int idGame, char *element, int IdElement){
  int numberOfResult, numberOfData, life, attack, idPlayer;
  char query[1000], ***data = NULL;
  if(strcmp(element, "PLAY") == 0){
    life = getPlayerLife(mysql, idGame, IdElement);
    if(life <= 0) return true;
    return false;
  }
  printf("death on board\n");
  fflush(stdout);
  getCardValue(mysql, idGame, IdElement, &life, &attack);
  printf("life of Card : %d attack of Card %d\n", life, attack);
  if(life <= 0){
    idPlayer = cardIdPlayerOnBoard(mysql, idGame, IdElement);
    sprintf(query,"DELETE FROM BOARD WHERE cardId = %d AND matchId = %d", IdElement, idGame);
    mysql_query(mysql, query);
    if(isSpecialCard(mysql, IdElement, ZOMBI)){
      printf("zombi action\n");
      drawCard(mysql, idGame, idPlayer);
    }
    return true;
  }
  return false;
}

int cardIdPlayerOnBoard(MYSQL *mysql, int idGame, int idCard){
  int numberOfResult, numberOfData, idPlayerTemp;
  char query[1000], ***data = NULL;

  sprintf(query,"SELECT idPlayer FROM BOARD WHERE cardId = %d AND matchId = %d", idCard, idGame);
  dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
  if(data == NULL) return -1;
  idPlayerTemp = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);

  return idPlayerTemp;
}


bool addValue(MYSQL *mysql, int idGame, int value, int idTarget, char *target){
  int numberOfResult, numberOfData;
  char query[1000], ***data = NULL;

  sprintf(query,"SELECT %s FROM BOARD WHERE cardId = %d AND matchId = %d", target, idTarget, idGame);
  dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
  if(data == NULL) return false;
  printf("lifeM : %d after : %d\n", atoi(data[0][0]), atoi(data[0][0]) + value);
  sprintf(query,"UPDATE BOARD SET %s = %d WHERE matchId = %d AND cardId = %d", target, atoi(data[0][0]) + value, idGame, idTarget);
  printf("%s\n", query);
  mysql_query(mysql, query);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return true;
}


/**
* vérifie si la carte est un serviteur
* @param  mysql  connexion mysql
* @param  idCard l'id de la carte à vérifier
* @return        retourne TRUE si la carte est un serviteur
*/
bool isServant(MYSQL *mysql, int idCard){
  char ***result = NULL;
  int length, numChamps, i, y;
  char query[1000];
  sprintf(query, "SELECT 1 FROM CARD WHERE cardId = %d AND spell IS NULL", idCard);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return false;

  freeThreeDimensionArrayChar(&result, length, numChamps);
  return true;
}



/**
* verifie s'il l'utilisateur possède assez d'open pour jouer une carte
* @param  mysql         connexion mysql
* @param  openRemaining le nombre d'open restants
* @param  idCard        l'id de la carte dans la bdd
* @return        retourne vrai si l'utilisateur possède assez d'open
*/
bool enoughOpen(MYSQL *mysql, int *openRemaining, int idCard, int idGame, int idPlayer){
  char ***result = NULL;
  int length, numChamps, costCard;
  char query[1000];
  sprintf(query, "SELECT openCost FROM CARD WHERE cardId = %d", idCard);
  dataPull(mysql, query, &result, &length, &numChamps);
  costCard = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);
  printf("!!!!!!!!!!!!!!!!!!!!!!!!!!openRemaining : %d AND costCard : %d compare : %d result : %d\n", *openRemaining, costCard, costCard <= *openRemaining, *openRemaining - costCard);
  if(costCard <= *openRemaining){
    *openRemaining -= costCard;
    updateOpenRemaining(mysql, *openRemaining, idGame, idPlayer);
    return true;
  }
  return false;
}


/**
* verifie si la carte peut attaquer
* @param  mysql          connexion mysql
* @param  idCardAttacker l'id de la carte qui attaque
* @param  idCardTarget   l'id de la carte qui subi
* @param  idGame         l'id de la partie dans la bdd
* @param  idPlayer       l'id de l'utilisateur en cours dans la bdd
* @return                [description]
*/
bool attack(MYSQL *mysql, int idCardAttacker, int idCardTarget, int idGame, int idPlayer){
  int idEnemy;
  printf("\nattackInit\n");
  fflush(stdout);
  if(!isNotSleep(mysql, idCardAttacker, idGame, idPlayer)) return false;
  printf("isnotsleep\n");
  fflush(stdout);
  idEnemy = getIdEnemy(mysql, idGame, idPlayer);
  printf("enemy is %d\n", idEnemy);
  fflush(stdout);
  if(idCardTarget == 0 && !otherCardAreSpecial(mysql, 0, DEFENSIVE, idEnemy, idGame)){
    printf("can attack the player\n");
    fflush(stdout);
    attackEnemy(mysql, idGame, idEnemy, idCardAttacker);
    printf("attack player success\n");
    fflush(stdout);
    sleepCard(mysql, idCardAttacker, idGame);
    printf("sleep card\n");
    fflush(stdout);
    return true;
  }
  else if(isSpecialCard(mysql, idCardTarget, DEFENSIVE) || !otherCardAreSpecial(mysql, idCardTarget, DEFENSIVE, idEnemy, idGame)){
    printf("can attack\n");
    fflush(stdout);
    attackEnemyCard(mysql, idCardAttacker, idGame, idCardTarget);
    sleepCard(mysql, idCardAttacker, idGame);
    return true;
  }

  return false;
}


void getCardValue(MYSQL *mysql, int idGame, int idCard, int *life, int *attack){
  char ***result = NULL;
  int length, numChamps, attackValue, idEnemy;
  char query[1000];

  sprintf(query, "SELECT CARD.life, BOARD.lifeModified, CARD.attack, BOARD.attackModified FROM CARD, BOARD WHERE CARD.cardId = %d AND BOARD.cardId = %d AND BOARD.matchId = %d", idCard, idCard, idGame);
  dataPull(mysql, query, &result, &length, &numChamps);
  *life = atoi(result[0][0]) + atoi(result[0][1]);
  *attack = atoi(result[0][2]) + atoi(result[0][3]);
  freeThreeDimensionArrayChar(&result, length, numChamps);
}

int getIdEnemy(MYSQL *mysql, int idGame, int idPlayer){
  char ***result = NULL;
  int length, numChamps, attackValue, idEnemy;
  char query[1000];

  sprintf(query, "SELECT idPlayer FROM PLAY WHERE matchId = %d AND idPlayer != %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL )return -1;
  idEnemy = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);
  return idEnemy;
}

int attackEnemy(MYSQL *mysql, int idGame, int idEnemy, int idCard){
  char ***result = NULL;
  int length, numChamps, attackValue, lifeValue;
  char query[1000];

  if(!isServant(mysql, idCard)) return -1;
  getCardValue(mysql, idGame, idCard, &lifeValue, &attackValue);
  sprintf(query, "SELECT playerLife FROM PLAY WHERE matchId = %d AND idPlayer = %d", idGame, idEnemy);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL )  return -1;
  attackValue = atoi(result[0][0]) - attackValue;
  freeThreeDimensionArrayChar(&result, length, numChamps);
  //if(attackValue <= 0) return 0;
  sprintf(query,"UPDATE PLAY SET playerLife = %d WHERE idPlayer = %d AND matchId = %d", attackValue, idEnemy, idGame);
  mysql_query(mysql, query);
  return 1;
}

int attackEnemyCard(MYSQL *mysql, int idCardAttacker, int idGame, int idCardTarget){

  char ***result = NULL;
  int length, numChamps, attackValueAttacker, attackValueTarget, lifeValueAttacker, lifeValueTarget, autodestruction = 0, idPlayer;
  char query[1000];
  printf("init attack card a: %d t : %d\n", idCardAttacker, idCardTarget);
  fflush(stdout);
  if(!isServant(mysql, idCardAttacker)) return -1;
  if(!isServant(mysql, idCardTarget)) return -1;
  printf("isServant\n");
  fflush(stdout);
  getCardValue(mysql, idGame, idCardAttacker, &lifeValueAttacker, &attackValueAttacker);
  getCardValue(mysql, idGame, idCardTarget, &lifeValueTarget, &attackValueTarget);
  printf("al : %d aa: %d tl : %d ta : %d\n", lifeValueAttacker, attackValueAttacker, lifeValueTarget, attackValueTarget);
  fflush(stdout);

  if(attackValueAttacker >= lifeValueTarget){
    printf("target die\n");
    fflush(stdout);
    if(isSpecialCard(mysql, idCardTarget, ZOMBI)){
      idPlayer = cardIdPlayerOnBoard(mysql, idGame, idCardTarget);
      printf("zombi action\n");
      drawCard(mysql, idGame, idPlayer);
    }

    sprintf(query,"DELETE FROM BOARD WHERE cardId = %d AND matchId = %d", idCardTarget, idGame);
    mysql_query(mysql, query);
    autodestruction++;
  }

  if(attackValueTarget >= lifeValueAttacker){
    printf("attacker die\n");
    fflush(stdout);
    if(isSpecialCard(mysql, idCardAttacker, ZOMBI)){
      idPlayer = cardIdPlayerOnBoard(mysql, idGame, idCardAttacker);
      printf("zombi action\n");
      drawCard(mysql, idGame, idPlayer);
    }

    sprintf(query,"DELETE FROM BOARD WHERE cardId = %d AND matchId = %d", idCardAttacker, idGame);
    mysql_query(mysql, query);
    autodestruction += 2;
  }

  switch(autodestruction){
    case 0:
    addValue(mysql, idGame, -attackValueTarget, idCardAttacker, "lifeModified");
    addValue(mysql, idGame, -attackValueAttacker, idCardTarget, "lifeModified");
    break;
    case 1:
    addValue(mysql, idGame, -attackValueTarget, idCardAttacker, "lifeModified");
    break;
    case 2:
    addValue(mysql, idGame, -attackValueAttacker, idCardTarget, "lifeModified");
    break;
  }

  return 1;

}


/**
* vérifie si le serviteur ne dort pas
* @param  mysql    connexion mysql
* @param  idCard   l'id de la carte à vérifier
* @param  idGame   l'id de la partie
* @param  idPlayer l'id de joueur possèdant la carte
* @return          retourne true si la carte ne dort pas
*/
bool isNotSleep(MYSQL *mysql, int idCard, int idGame, int idPlayer){
  int length, numChamps, sleep;
  char query[1000], ***result = NULL;
  printf("\nisNotspellBigining \n");
  fflush(stdout);
  sprintf(query, "SELECT sleep FROM BOARD WHERE cardId = %d AND matchId = %d AND idPlayer = %d", idCard, idGame, idPlayer);
  printf("%s\n", query);
  fflush(stdout);
  printf("query end length %d\n", length);
  fflush(stdout);
  dataPull(mysql, query, &result, &length, &numChamps);
  printf("datapull end \n");
  fflush(stdout);
  if(result == NULL) return false;
  printf("sleep = %s \n", result[0][0]);
  fflush(stdout);
  sleep = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);
  printf("end \n");
  fflush(stdout);
  if(!sleep) return true;

  return false;
}


/**
* enlêve le statut endormi à toutes les cartes sur board qui le possèdent
* @param  mysql    connexion mysql_init
* @param  idGame   l'id de la partie
* @param  idPlayer l'id du idPlayer
*/
void unSleep(MYSQL *mysql, int idGame, int idPlayer){
  char ***result = NULL;
  int length, numChamps, i;
  char query[1000];
  sprintf(query, "SELECT cardId FROM BOARD WHERE sleep = 1 AND matchId = %d AND idPlayer = %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);

  for(i = 0; i < length; i++){
    sprintf(query,"UPDATE BOARD SET sleep = 0 WHERE cardId = %d AND matchId = %d AND idPlayer = %d", atoi(result[i][0]), idGame, idPlayer);
    mysql_query(mysql, query);
  }
  if(result != NULL) freeThreeDimensionArrayChar(&result, length, numChamps);
}


void sleepCard(MYSQL *mysql, int idCard, int idGame){
  char query[1000];
  sprintf(query,"UPDATE BOARD SET sleep = 1 WHERE cardId = %d AND matchId = %d", idCard, idGame);
  mysql_query(mysql, query);
}


bool otherCardAreSpecial(MYSQL *mysql, int idCard, int specialEffect, int player, int idGame){
  char ***result = NULL, ***card = NULL;
  int length, numChamps, i, lengthCard, numChampsCard;
  char query[1000];
  sprintf(query, "SELECT cardId FROM BOARD WHERE cardId != %d AND matchId = %d AND idPlayer = %d", idCard, idGame, player);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return false;

  for(i = 0; i < length; i++){
    if(isSpecialCard(mysql, atoi(result[i][0]), specialEffect)){
      freeThreeDimensionArrayChar(&result, length, numChamps);
      return true;
    }
  }
  freeThreeDimensionArrayChar(&result, length, numChamps);
  return false;
}
/**
* Vérifie si la carte est une carte de type specialEffect
* @param  mysql  connexion mysql
* @param  idCard L'id de la carte
*  @param  specialEffect type d'effet
* @return        retourne vrai si la carte est une carte de type specialEffect
*/
bool isSpecialCard(MYSQL *mysql, int idCard, int specialEffect){
  int length, numChamps, i, y;
  char query[1000], ***result = NULL;

  sprintf(query, "SELECT 1 FROM CARD WHERE cardId = %d AND specialEffect = %d", idCard, specialEffect);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return false;
  else{
    freeThreeDimensionArrayChar(&result, length, numChamps);
    return true;
  }
}


int endGame(MYSQL *mysql, int idGame, int idPlayer){
  int idEnemy, playerLife, playerEnemyLife;

  idEnemy = getIdEnemy(mysql, idGame, idPlayer);
  if(!countCard(mysql, idGame, idPlayer) && !countCard(mysql, idGame, idEnemy)){
    playerLife = getPlayerLife(mysql, idGame, idPlayer);
    playerEnemyLife = getPlayerLife(mysql, idGame, idEnemy);
    if(playerLife > playerEnemyLife) return idEnemy;
    if(playerLife < playerEnemyLife) return idPlayer;
    return 0;
  }

  if(!countCard(mysql, idGame, idPlayer) || death(mysql, idGame, "PLAY", idPlayer)){
    return idPlayer;
  }


  if(!countCard(mysql, idGame, idEnemy) || death(mysql, idGame, "PLAY", idEnemy)){
    return idEnemy;
  }

  return -1;
}


int getPlayerLife(MYSQL *mysql, int idGame, int idPlayer){
  int length, numChamps, playerLife = 0;
  char query[1000], ***result = NULL;

  sprintf(query, "SELECT playerlife FROM PLAY WHERE matchId = %d AND idPlayer = %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return -1;

  playerLife = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);

  return playerLife;
}


int countCard(MYSQL *mysql, int idGame, int idPlayer){
  int length, numChamps, count = 0;
  char query[1000], ***result = NULL;

  sprintf(query, "SELECT 1 FROM DECK WHERE matchId = %d AND idPlayer = %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  count = length;

  sprintf(query, "SELECT 1 FROM HAND WHERE matchId = %d AND idPlayer = %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  count += length;

  sprintf(query, "SELECT 1 FROM BOARD WHERE matchId = %d AND idPlayer = %d", idGame, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  count += length;

  freeThreeDimensionArrayChar(&result, length, numChamps);
  return count;
}


void finishGame(MYSQL *mysql, int idGame, int loser){
  char query[1000];
  int winner;

  if(loser == 0){
    sprintf(query,"UPDATE GAME SET winnerId = %d WHERE matchId = %d", 0, idGame);
    mysql_query(mysql, query);
    return;
  }
  winner = getIdEnemy(mysql, idGame, loser);
  printf("winner %d loser %d\n", winner, loser);
  fflush(stdout);
  sprintf(query,"UPDATE GAME SET winnerId = %d WHERE matchId = %d", winner, idGame);
  mysql_query(mysql, query);
  sprintf(query,"UPDATE GAME SET loserId = %d WHERE matchId = %d", loser, idGame);
  mysql_query(mysql, query);

  printf("winner\n");
  fflush(stdout);
  incDataPlayer(mysql, loser, "loose");
  incDataPlayer(mysql, winner, "win");
  printf("end\n");
  fflush(stdout);
}

int gameIsEnd(MYSQL *mysql, int idGame){
  int length, numChamps, isEnd;
  char query[1000], ***result = NULL;

  printf("test gameisEnd\n");
  fflush(stdout);
  sprintf(query, "SELECT winnerId FROM GAME WHERE matchid = %d AND winnerId IS NOT NULL", idGame);
  printf("test gameisEnd query end\n");
  fflush(stdout);
  dataPull(mysql, query, &result, &length, &numChamps);
  printf("test gameisEnd dataPull end %d\n", length);
  fflush(stdout);
  if(result == NULL) return -1;

  printf("test game END\n");
  fflush(stdout);
  isEnd = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);

  sprintf(query,"DELETE FROM HAND WHERE matchId = %d", idGame);
  mysql_query(mysql, query);

  sprintf(query,"DELETE FROM DECK WHERE matchId = %d", idGame);
  mysql_query(mysql, query);

  sprintf(query,"DELETE FROM BOARD WHERE matchId = %d", idGame);
  mysql_query(mysql, query);

  sprintf(query,"DELETE FROM PLAY WHERE matchId = %d", idGame);
  mysql_query(mysql, query);

  sprintf(query,"DELETE FROM GAME WHERE matchId = %d", idGame);
  mysql_query(mysql, query);

  return isEnd;


}


void incDataPlayer(MYSQL *mysql, int idPlayer, char *data){
  int length, numChamps, oldValue;
  char query[1000], ***result = NULL;

  sprintf(query, "SELECT  %s FROM PLAYER WHERE idPlayer = %d", data, idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return;

  oldValue = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);

  oldValue++;
  sprintf(query,"UPDATE PLAYER SET %s = %d WHERE idPlayer = %d", data, oldValue, idPlayer);
  mysql_query(mysql, query);

}
