#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <errno.h>
#include <winsock.h>
#include <time.h>
#include "MYSQL/mysql.h"
#include "database.h"
#include "enum.h"
#include "game.h"
#include "usefull.h"


int CardPlayable(MYSQL *mysql, int idPlayer, int idGame, int AvailableOpen){
  char query[1000];
  char ***data = NULL;
  int numberOfResult, numberOfData, cardId;

  if (!emptySlot(mysql, idGame, idPlayer,"BOARD")) {
    return -1;
  }

  sprintf(query, "SELECT HAND.cardId FROM HAND, CARD WHERE CARD.cardId = HAND.cardId AND HAND.try = 0 AND CARD.openCost <= %d AND HAND.idPlayer = %d AND HAND.matchId = %d ORDER BY RAND() LIMIT 1", AvailableOpen, idPlayer, idGame);
  dataPull(mysql, query, &data, &numberOfResult, &numberOfData);
  if(data == NULL){
    return -1;
  }
  cardId = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return cardId;
}

int lessLife(MYSQL *mysql, int idPlayer, int idGame){
  char query[1000];
  char ***data = NULL;
  int lessLife, numberOfResult, numberOfData;

  sprintf(query, "SELECT CARD.cardId FROM CARD, BOARD WHERE CARD.cardId = BOARD.cardId AND CARD.life + BOARD.lifeModified IN(SELECT MIN(life + lifeModified) FROM CARD, BOARD WHERE idPlayer = %d AND matchId = %d ORDER by RAND())",idPlayer, idGame);
  dataPull(mysql, query, &data, &numberOfResult, &numberOfData);
  if(data == NULL){
    return -1;
  }
  lessLife = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
  return lessLife;
}


bool IaPlayCard(MYSQL *mysql,int idPlayer ,int idGame, int *AvailableOpen){
  char query[1000];
  char ***data = NULL;
  int idCard, numberOfResult, numberOfResult, targetAddAtt, targetAtt, Life;

  if(AvailableOpen == NULL) return false;
  while ((idCard = CardPlayable(mysql, idPlayer, idGame, *AvailableOpen)) != -1) {
    if(isServant(mysql,idCard)){
      playCard(mysql, idGame, idPlayer, idCard, 0);
    }else{
      //récupérer dans la base de données le type de la carte puis if(spell ==1)alors machin
      sprintf(query,"SELECT spell, spellEffect FROM CARD WHERE cardId = %d", idGame, idPlayer);
      dataPull(mysql,query , &data, &numberOfResult, &numberOfData);
      if(dataPull == NULL) return false;
      spell = atoi(data[0][0]);
      value = atoi(data[0][0]);

      if(spell == ADDLIFE){
        Life = lesslife(*mysql, idPlayer, idGame);
          if(life != -1){
            playSpell(mysql, idGame, idPlayer, idCard, lessLife);
            return true;
          }else{
            return false;
          }
        }

      }
      if(spell == ADDATTACK){
        sprintf(query,"SELECT CARD.cardId FROM CARD, BOARD WHERE idPlayer = %d AND matchId = %d ORDER BY RAND() LIMIT 1", idPlayer, idGame);
        dataPull(mysql, query, &data, &numberOfResult, &numberOfData);
        if(data == NULL){
          return false;
        }
        targetAddAtt = atoi(data[0][0]);
        playSpell(mysql, idGame, idPlayer, idCard, targetAddAtt);
        freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
        return true;
      }
      if(spell == DRAWCARD){
        playSpell(mysql, idGame, idPlayer, idCard, 0);
      }
      if(spell == ATTACK){
        sprintf(query, "SELECT CARD.cardId FROM CARD, BOARD WHERE CARD.cardId = BOARD.cardId AND CARD.life + BOARD.lifeModified IN(SELECT MAX(life + lifeModified) FROM CARD, BOARD WHERE idPlayer = %d AND matchId = %d )",idPlayer, idGame);
        dataPull(mysql, query, &data, &numberOfResult, &numberOfData);
        if(data == NULL){
          return -1;
        }
        lessLife = atoi(data[0][0]);
        freeThreeDimensionArrayChar(&data, numberOfResult, numberOfData);
      }
    }
    sprintf(query, "UPDATE HAND SET try = 0 WHERE try = 1");
    mysql_query(mysql, query);
  }
