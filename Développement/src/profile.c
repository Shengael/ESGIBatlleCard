#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_rotozoom.h>
#include <string.h>
#include <tchar.h>
#include <winsock.h>
#include <time.h>
#include <MYSQL/mysql.h>
#include <database.h>
#include <display.h>
#include <game.h>
#include <gameEvent.h>
#include <SDL_write.h>
#include <profile.h>


bool verifConnection(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int numberRow, numberData;
  char ***data = NULL, query[1000], pwd[25];

  sprintf(query,"SELECT idPlayer FROM PLAYER WHERE nickname = '%s' AND password = '%s'", player->pseudo, player->passWord);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data == NULL) return false;
  player->idPlayer = atoi(data[0][0]);

  return true;
}


bool registration(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int numberRow, numberData;
  char ***data = NULL, query[1000], pwd[25];

  sprintf(query,"SELECT 1 FROM PLAYER WHERE nickname = '%s'", player->pseudo);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data != NULL){
    freeThreeDimensionArrayChar(&data, numberRow, numberData);
    return false;
  }

  if(strcmp(player->passWord, player->vPassword) != 0) return false;
  sprintf(query,"INSERT INTO PLAYER(nickname, password) VALUES('%s', '%s')", player->pseudo, player->passWord);
  mysql_query(mysql, query);
  return true;
}

void displayPassword(char *password, char **temp){
  int size = strlen(password), i;
  (*temp)[0] = '\0';

  for(i = 0; i < size; i++){
    strcat(*temp, "*");
  }
}
