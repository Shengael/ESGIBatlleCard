#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <errno.h>
#include <winsock.h>
#include <time.h>
#include "MYSQL/mysql.h"
#include "database.h"
#include "enum.h"
#include "game.h"
#include "usefull.h"


int registrationTournament(MYSQL *mysql, int idPlayer, int idTournament){
  char query[1000];
  int numberRow, numberData, error;
  if(error = playerAlreadyInTournament(mysql, idPlayer)) return error;
  sprintf(query, "INSERT INTO ASK(idPlayer,  tournamentId) VALUES(%d, %d)", idPlayer, idTournament);
  mysql_query(mysql, query);
  sprintf(query, "UDPATE PLAYER SET currentTournament = %d WHERE idPlayer = %d", idTournament, idPlayer);
  mysql_query(mysql, query);
  return 1;
}

int tournamentCreation(MYSQL *mysql, int idPlayer, int playerNumber){
  char query[1000], ***data, idTournament;
  int numberRow, numberData, error;

  if(error = playerAlreadyInTournament(mysql, idPlayer)) return error;

  sprintf(query, "INSERT INTO TOURNAMENT(playerNumber, idPlayer) VALUES(%d, %d)", playerNumber, idPlayer);
  mysql_query(mysql, query);
  sprintf(query, "SELECT  tournamentId FROM tournamentId WHERE idPlayer = %d", idPlayer);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data == NULL) return -1;
  idTournament = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberRow, numberData);

  return idTournament;
}

int playerAlreadyInTournament(MYSQL *mysql, int idPlayer){
  int numberRow, numberData, result;
  char  ***data, query[1000];
  sprintf(query, "SELECT  currentTournament FROM PLAYER WHERE idPlayer = %d", idPlayer);
  dataPull(mysql, query , &data, &numberRow, &numberData);
  if(data == NULL) return -1;
  result = atoi(data[0][0]);
  freeThreeDimensionArrayChar(&data, numberRow, numberData);
  if(!result) return 0;
  return 1;
}
