/**
* @Author: Luis VALDEZ
* @Date:   2017-12-17T00:19:39+01:00
* @Email:  valdez.jds.16@gmail.com
* @Project: ESGIBATTLECARD
* @Last modified by:   Luis VALDEZ
* @Last modified time: 2017-12-18T12:05:24+01:00
* @Copyright: Shengael
*/
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_rotozoom.h>
#include <string.h>
#include <tchar.h>
#include <winsock.h>
#include <time.h>
#include <MYSQL/mysql.h>
#include <database.h>
#include <display.h>
#include <game.h>
#include <gameEvent.h>
#include <SDL_write.h>

SetupSdl *initDisplay(){
  Uint32 coul, coul1;
  int i, xPosition;
  double yResize, xResize;
  SetupSdl *sdlConf;
  SDL_Surface *screen;
  const SDL_VideoInfo *resolution;
  SDL_Init(SDL_INIT_VIDEO);
  TTF_Init();
  SDL_EnableUNICODE(1);
  resolution = SDL_GetVideoInfo();

  sdlConf = malloc(sizeof(SetupSdl));
  sdlConf->screen = SDL_SetVideoMode(resolution->current_w, resolution->current_h, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN);
  xResize = resolution->current_w / 1920.0;
  yResize = resolution->current_h / 1080.0;
  sdlConf->position.x = 0;
  sdlConf->position.y = 0;
  sdlConf->endDisplay = 1;
  sdlConf->color.r = 0;
  sdlConf->color.g = 0;
  sdlConf->color.b = 0;
  sdlConf->error.r = 255;
  sdlConf->error.g = 0;
  sdlConf->error.b = 0;

  for(i = 0; i < 16; i += 2){
    xPosition = (i / 2) * (220 * xResize) + (122 * xResize);
    sdlConf->xHand[i] = xPosition;
    sdlConf->xHand[i + 1] = xPosition + (195 * xResize);

    xPosition = (1612 * xResize) - (i / 2) * (220 * xResize);
    sdlConf->xHandEnemy[i] = xPosition;
    sdlConf->xHandEnemy[i + 1] = xPosition + (195 * xResize);


    if(i < 12){
      xPosition = (i / 2) * (220 * xResize) + (542 * xResize);
      sdlConf->xBoard[i] = xPosition;
      sdlConf->xBoard[i + 1] = xPosition + (195 * xResize);

      xPosition = (1201 * xResize) - ((i / 2) * (220 * xResize));
      sdlConf->xBoardEnemy[i] = xPosition;
      sdlConf->xBoardEnemy[i + 1] = xPosition + (195 * xResize);
    }

  }

  sdlConf->yHand[0] = (1015 * yResize);
  sdlConf->yHand[1] = (1080 * yResize);
  sdlConf->yHandEnemy[0] = (0 * yResize);
  sdlConf->yHandEnemy[1] = (65 * yResize);
  sdlConf->yBoard[0] = (601 * yResize);
  sdlConf->yBoard[1] = (875 * yResize);
  sdlConf->yBoardEnemy[0] = (243 * yResize);
  sdlConf->yBoardEnemy[1] = (517 * yResize);
  sdlConf->focusWrite = 0;
  sdlConf->xResize = yResize;
  sdlConf->yResize = yResize;

  strcpy(sdlConf->pathOfPicture, "images/cardImages/card/");
  strcpy(sdlConf->pathOfTiny, "images/cardImages/min/");

  sdlConf->police = TTF_OpenFont("polices/calibri.ttf", 30);
  sdlConf->policeText = TTF_OpenFont("polices/calibri.ttf", 50);
  sdlConf->policeDeck = TTF_OpenFont("polices/calibri.ttf", 40);
  if(sdlConf->police == NULL){
    exit(EXIT_FAILURE);
  }
  SDL_WM_SetCaption("ESGI BATTLE CARD", NULL);
  return sdlConf;
}


void eventManagement(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int  endTurn = 1;
  while(sdlConf->endDisplay){

    SDL_WaitEvent(&sdlConf->event);

    switch (player->eventStatus) {
      case 0:
      initGame(sdlConf, player);
      break;
      case 1:
      connectionEvent(mysql, sdlConf, player);
      break;
      case 2:
      registrationEvent(mysql, sdlConf, player);
      break;
      case 3:
      indexEvent(sdlConf, player);
      break;
      case 4:
      eventSearch(mysql, sdlConf, player);
      break;
      case 5:
      eventInGame(mysql, sdlConf, player);
      break;
      case 6:
      eventEndGame(player, sdlConf);
      break;
      case 7:
      break;
    }
    eventDisplay(sdlConf, player);
  }
}


void initGame(SetupSdl *sdlConf, playerData *player){
  displayFullScreen(sdlConf, "images/menu_accueil/connexionInscri.bmp");
  SDL_Flip(sdlConf->screen);
  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){

      if(checkRect(sdlConf, 771, 1170, 287, 427)){
        player->eventStatus = 1;
        displayFullScreen(sdlConf, "images/menu_accueil/Connexion.bmp");
        SDL_Flip(sdlConf->screen);
      }

      else if(checkRect(sdlConf, 779, 1177, 596, 735)){
        player->eventStatus = 2;
        displayFullScreen(sdlConf, "images/menu_accueil/inscription.bmp");
        SDL_Flip(sdlConf->screen);
      }
    }
    break;
  }
}

void connectionEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  char inputKey, *temp;

  temp = malloc(25);
  displayPassword(player->passWord, &temp);
  if(sdlConf->focusWrite == 1) inputData(&player->pseudo, sdlConf, 25);
  if(sdlConf->focusWrite == 2) inputData(&player->passWord, sdlConf, 25);
  displayFullScreen(sdlConf, "images/menu_accueil/Connexion.bmp");
  displayText(player->pseudo, sdlConf, 695 * sdlConf->xResize, 490 * sdlConf->yResize, 1);
  displayText(temp, sdlConf, 695 * sdlConf->xResize, 742 * sdlConf->yResize, 1);
  displayText(sdlConf->errorText, sdlConf, 790 * sdlConf->xResize, 280 * sdlConf->yResize, 0);
  SDL_Flip(sdlConf->screen);
  free(temp);
  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){
      if(checkRect(sdlConf, 683, 1263, 449, 543)){
        sdlConf->focusWrite = 1;
      }
      else if(checkRect(sdlConf, 684, 1264, 701, 795)){
        sdlConf->focusWrite = 2;
      }
      else sdlConf->focusWrite = 0;
    }
    break;

    case SDL_KEYDOWN:
    inputKey = sdlConf->event.key.keysym.unicode;
    if(inputKey == 13){
      if(verifConnection(mysql, sdlConf, player)){
        displayFullScreen(sdlConf, "images/menu_accueil/fondsAccueil.bmp");
        SDL_Flip(sdlConf->screen);
        player->eventStatus = 3;
        if(playerInGame(mysql, player->idPlayer)){
          player->idGame = getIdGame(mysql, player->idPlayer);
          updateBoard(mysql, sdlConf, player);
          player->eventStatus = 5;
        }
        sdlConf->focusWrite = 0;
        sdlConf->errorText[0] = '\0';
      }
      else{
        strcpy(sdlConf->errorText, "Les identifiants n'existent pas");
        sdlConf->focusWrite = 0;
      }
    }
<<<<<<< HEAD
    if(mysql == NULL) displayImage(screen, "images/test.bmp", 0, 0);
    else displayImage(screen, "images/game_start.bmp", 0, 0);
    
    SDL_WaitEvent(&event);
    if(*eventStatus == 0) idGame = eventSearch(mysql, event, &endDisplay, screen, idPlayer, eventStatus);
    //else if(*eventStatus == 1) displayBoard(screen);
    eventDisplay(event, &endDisplay, screen);
    //eventInGame(event, &endDisplay, screen);
=======
    break;
>>>>>>> 99dcef1c86e5680959c0ea45764c73087ad3a8ef
  }
}


void registrationEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  char inputKey;
  char *temp, *vTemp;
  temp = malloc(25);
  vTemp = malloc(25);
  displayPassword(player->passWord, &temp);
  displayPassword(player->vPassword, &vTemp);

  if(sdlConf->focusWrite == 1) inputData(&player->pseudo, sdlConf, 25);
  if(sdlConf->focusWrite == 2) inputData(&player->passWord, sdlConf, 25);
  if(sdlConf->focusWrite == 3) inputData(&player->vPassword, sdlConf, 25);
  displayFullScreen(sdlConf, "images/menu_accueil/inscription.bmp");
  displayText(player->pseudo, sdlConf, 695 * sdlConf->xResize, 490 * sdlConf->yResize, 1);
  displayText(temp, sdlConf, 695 * sdlConf->xResize, 742 * sdlConf->yResize, 1);
  displayText(vTemp, sdlConf, 695 * sdlConf->xResize, 910 * sdlConf->yResize, 1);
  displayText(sdlConf->errorText, sdlConf, 790 * sdlConf->xResize, 280 * sdlConf->yResize, 0);
  SDL_Flip(sdlConf->screen);

  free(temp);
  free(vTemp);
  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){
      if(checkRect(sdlConf, 684, 1263, 449, 543)){
        sdlConf->focusWrite = 1;
      }
      else if(checkRect(sdlConf, 684, 1263, 701, 794)){
        sdlConf->focusWrite = 2;
      }
      else if(checkRect(sdlConf, 687, 1267, 869, 972)){
        sdlConf->focusWrite = 3;
      }
      else sdlConf->focusWrite = 0;
    }
    break;

    case SDL_KEYDOWN:
    inputKey = sdlConf->event.key.keysym.unicode;
    if(inputKey == 13){
      if(registration(mysql, sdlConf, player)){
        player->eventStatus = 1;
        sdlConf->focusWrite = 0;
        player->pseudo[0] = '\0';
        player->passWord[0] = '\0';
        player->vPassword[0] = '\0';
        sdlConf->errorText[0] = '\0';
        displayFullScreen(sdlConf, "images/menu_accueil/Connexion.bmp");
        SDL_Flip(sdlConf->screen);
      }
      else{
        strcpy(sdlConf->errorText, "Il y a des erreurs");
        sdlConf->focusWrite = 0;
      }
    }
    break;
  }

}

void eventEndGame(playerData *player, SetupSdl *sdlConf){

  displayEndGame(sdlConf, player->win);

  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){

      if(checkRect(sdlConf, 896, 1096, 872, 942)){
        player->eventStatus = 3;
      }
    }
    break;
  }

}

void indexEvent(SetupSdl *sdlConf, playerData *player){
  displayFullScreen(sdlConf, "images/menu_accueil/fondsAccueil.bmp");
  SDL_Flip(sdlConf->screen);

  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){

      if(checkRect(sdlConf, 782, 1181, 272, 412)){
        player->eventStatus = 4;
        displaySearch(sdlConf);
      }
      else if(checkRect(sdlConf, 778, 1177, 538, 678)){
        //player->eventStatus = 4;
      }
      else if(checkRect(sdlConf, 779, 1178, 804, 944)){

      }
    }
    break;
  }

}


void eventDisplay(SetupSdl *sdlConf, playerData *player){
  switch(sdlConf->event.type){
    case SDL_KEYDOWN:
    switch(sdlConf->event.key.keysym.unicode)
    {
      case SDLK_q:
      sdlConf->endDisplay = 0;
      player->endTurn = 0;
      player->endGame = 0;
      break;
    }
    break;
  }
}

void eventSearch(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  displaySearch(sdlConf);

  switch(sdlConf->event.type){
    case SDL_MOUSEBUTTONUP:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){
      if(checkRect(sdlConf, 771, 1169, 330, 470)){
        displayLoading(sdlConf);
        if((player->idGame = searchGame(mysql,player->idPlayer)) != -1){
          player->eventStatus = 5;
          updateBoard(mysql, sdlConf, player);
        }
      }
    }
    break;
  }
}

void displayBoard(SetupSdl *sdlConf, int deck){
  if(deck){
    displayFullScreen(sdlConf, "images/boards/boardStud.bmp");
  }
  else{
    displayFullScreen(sdlConf, "images/boards/boardProf.bmp");
  }
}

void displayEndGame(SetupSdl *sdlConf, int win){
  switch(win){
    case 0:
    displayFullScreen(sdlConf, "images/menu_accueil/endGame/defaite.bmp");
    break;
    case 1:
    displayFullScreen(sdlConf, "images/menu_accueil/endGame/victoire.bmp");
    break;
    case 2:
    displayFullScreen(sdlConf, "images/menu_accueil/endGame/victoire.bmp");
    break;
  }
  SDL_Flip(sdlConf->screen);
}

void displayLoading(SetupSdl *sdlConf){
  displayFullScreen(sdlConf, "images/game_start.bmp");
  SDL_Flip(sdlConf->screen);
}

void displaySearch(SetupSdl *sdlConf){
  displayFullScreen(sdlConf, "images/menu_accueil/fondsPARTIE.bmp");
  SDL_Flip(sdlConf->screen);
}

void displayFullScreen(SetupSdl *sdlConf, char *pathImage){
  SDL_Surface *imageDeFond = NULL, *screen = sdlConf->screen;
  SDL_Rect position = sdlConf->position;
  imageDeFond = SDL_LoadBMP(pathImage);
  imageDeFond = zoomSurface(imageDeFond, sdlConf->xResize, sdlConf->yResize, 1);
  SDL_BlitSurface(imageDeFond, NULL, screen, &position);
  //SDL_Flip(screen);
  SDL_FreeSurface(imageDeFond);
}

void displayCard(SetupSdl *sdlConf, char *pathImage, int x, int y, int size){
  SDL_Surface *imageDeFond = NULL, *screen = sdlConf->screen;
  SDL_Rect position;
  double sizeInitX, sizeInitY;
  position.x = x;
  position.y = y;
  imageDeFond = SDL_LoadBMP(pathImage);

  if(!size){
    sizeInitX = 195 / 397.0;
    sizeInitY = 64 / 168.0;
  }
  else if(size == 1){
    sizeInitX = 195 / 300.0;
    sizeInitY = 274 / 450.0;
  }
  else{
    sizeInitX = 1;
    sizeInitY = 1;
  }

  imageDeFond = zoomSurface(imageDeFond, sizeInitX * sdlConf->xResize, sizeInitY * sdlConf->yResize, 1);
  SDL_BlitSurface(imageDeFond, NULL, screen, &position);
  //  SDL_Flip(screen);
  SDL_FreeSurface(imageDeFond);
}



void updateBoard(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  char query[1000], ***data = NULL, ***hand = NULL, ***board = NULL, ***boardEnemy = NULL, path[100], numberDeckEnemy[3], ***lifePlayer = NULL;
  int numberOfEnemyCard, idEnemy, line, column, lineHand, columnHand, lineBoard, columnBoard, lineBoardEnemy, columnBoardEnemy, lineLife, columnLife, i, y;

  idEnemy = getIdEnemy(mysql, player->idGame, player->idPlayer);
  sprintf(query, "SELECT COUNT(cardId) FROM HAND WHERE idPlayer = %d AND matchId = %d UNION SELECT COUNT(cardId) FROM DECK WHERE idPlayer = %d AND matchId = %d", idEnemy, player->idGame, idEnemy, player->idGame);
  dataPull(mysql, query, &data, &line, &column);
  if(data == NULL) exit(EXIT_FAILURE);

  strcpy(numberDeckEnemy, data[1][0]);
  numberOfEnemyCard = atoi(data[0][0]);

  sprintf(query, "SELECT cardId FROM CARD WHERE cardId IN (SELECT cardId FROM HAND WHERE idPlayer = %d AND matchId = %d) ORDER BY cardId",  player->idPlayer, player->idGame);
  dataPull(mysql, query, &hand, &lineHand, &columnHand);

  sprintf(query, "SELECT cardId FROM CARD WHERE cardId IN (SELECT cardId FROM BOARD WHERE idPlayer = %d AND matchId = %d) ORDER by cardId", player->idPlayer, player->idGame);
  dataPull(mysql, query, &board, &lineBoard, &columnBoard);

  sprintf(query, "SELECT cardId FROM CARD WHERE cardId IN (SELECT cardId FROM BOARD WHERE idPlayer = %d AND matchId = %d) ORDER BY cardId", idEnemy, player->idGame);
  dataPull(mysql, query, &boardEnemy, &lineBoardEnemy, &columnBoardEnemy);

  sprintf(query, "SELECT COUNT(1) FROM DECK WHERE idPlayer = %d AND matchId = %d",  player->idPlayer, player->idGame);
  dataPull(mysql, query, &data, &line, &column);

  sprintf(query, "SELECT idPlayer, playerLife FROM PLAY WHERE idPlayer = %d OR idPlayer = %d",  player->idPlayer, idEnemy);
  dataPull(mysql, query, &lifePlayer, &lineLife, &columnLife);

  displayBoard(sdlConf, isStudent(mysql, player->idPlayer, player->idGame));
  displayText(data[0][0], sdlConf, 390 * sdlConf->xResize, 640 * sdlConf->yResize, 3);

  printf("%s %s %s %s\n", lifePlayer[0][0], lifePlayer[0][1], lifePlayer[1][0], lifePlayer[1][1]);
  fflush(stdout);
  if(atoi(lifePlayer[0][0]) == player->idPlayer){
    displayText(lifePlayer[0][1], sdlConf, 277 * sdlConf->xResize, 755 * sdlConf->yResize, 2);
    displayText(lifePlayer[1][1], sdlConf, 1601 * sdlConf->xResize, 327 * sdlConf->yResize, 2);
  }
  else{
    displayText(lifePlayer[1][1], sdlConf, 277 * sdlConf->xResize, 755 * sdlConf->yResize, 2);
    displayText(lifePlayer[0][1], sdlConf, 1601 * sdlConf->xResize, 327 * sdlConf->yResize, 2);
  }


  displayText(numberDeckEnemy, sdlConf, 1455 * sdlConf->xResize, 395 * sdlConf->yResize, 3);
  displayOpen(mysql, sdlConf, player);
  printf("end display text\n");
  fflush(stdout);


  //display for me
  for(i = 0; i < lineHand; i++){
    sprintf(path, "%s%s.bmp", sdlConf->pathOfTiny, hand[i][0]);
    displayCard(sdlConf, path, sdlConf->xHand[i * 2], sdlConf->yHand[0], 0);
  }
  printf("end hand card\n");
  fflush(stdout);
  for(i = 0; i < lineBoard; i++){
    sprintf(path, "%s%s.bmp", sdlConf->pathOfPicture, board[i][0]);
    displayCard(sdlConf, path, sdlConf->xBoard[i * 2], sdlConf->yBoard[0], 1);
    displayData(mysql, sdlConf, player, atoi(board[i][0]), sdlConf->xBoard[i * 2] , sdlConf->yBoard[1]);
  }
  printf("end board card\n");
  fflush(stdout);
  //DISPLAY FOR ENEMY
  for(i = 0; i < lineBoardEnemy; i++){
    sprintf(path, "%s%s.bmp", sdlConf->pathOfPicture, boardEnemy[i][0]);
    displayCard(sdlConf, path, sdlConf->xBoardEnemy[i * 2], sdlConf->yBoardEnemy[0], 1);
    displayData(mysql, sdlConf, player, atoi(boardEnemy[i][0]), sdlConf->xBoardEnemy[i * 2] , sdlConf->yBoardEnemy[1]);
  }

  printf("end board Enemy card\n");
  fflush(stdout);
  for(i = 0; i < numberOfEnemyCard; i++){
    displayCard(sdlConf, "images/cardImages/dosCartePt.bmp", sdlConf->xHandEnemy[i * 2], sdlConf->yHandEnemy[0], 0);
  }

  printf("end hand enemy card\n");
  fflush(stdout);

  SDL_Flip(sdlConf->screen);
  //freeThreeDimensionArrayChar(data, line, column);
  if(data != NULL) freeThreeDimensionArrayChar(&data, line, column);
  if(hand != NULL) freeThreeDimensionArrayChar(&hand, lineHand, columnHand);
  if(board != NULL) freeThreeDimensionArrayChar(&board, lineBoard, columnBoard);
  if(boardEnemy != NULL) freeThreeDimensionArrayChar(&boardEnemy, columnBoardEnemy, columnBoardEnemy);
  if(lifePlayer != NULL) freeThreeDimensionArrayChar(&lifePlayer, lineLife, columnLife);
  printf("end display update\n");
  fflush(stdout);
}


void displayData(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard, int x, int y){
  int life, attack;
  char value[3];
  getCardValue(mysql, player->idGame, idCard, &life, &attack);
  sprintf(value, "%d", attack);
  displayText(value, sdlConf, x + 35 * sdlConf->xResize, y - 50 * sdlConf->yResize, 2);
  sprintf(value, "%d", life);
  displayText(value, sdlConf, x + 151 * sdlConf->xResize, y - 50 * sdlConf->yResize, 2);
}


void displayOpen(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int i, xPosition, length, numChamps, openEnemy;
  char query[1000], ***result = NULL;

  sprintf(query, "SELECT openRemaining FROM PLAY WHERE matchId = %d AND idPlayer != %d", player->idGame, player->idPlayer);
  dataPull(mysql, query, &result, &length, &numChamps);
  if(result == NULL) return;
  openEnemy = atoi(result[0][0]);
  freeThreeDimensionArrayChar(&result, length, numChamps);


  if(!itIsMyTurn(mysql, player->idGame, player->idPlayer)){
    displayCard(sdlConf, "images/boards/cachePASSER.bmp", 875 * sdlConf->xResize, 524 * sdlConf->yResize, 2);
  }

  for(i = 0; i < 10 - player->open; i++){
    xPosition = ((517 * sdlConf->xResize) - (50 * i * sdlConf->xResize)) - 42 * sdlConf->xResize;
    displayCard(sdlConf, "images/boards/cacheOPEN.bmp", xPosition, 910 * sdlConf->yResize, 2);
  }

  for(i = 0; i < 10 - openEnemy; i++){
    xPosition = ((1893 * sdlConf->xResize) - (50 * i * sdlConf->xResize)) - 42 * sdlConf->xResize;
    displayCard(sdlConf, "images/boards/cacheOPEN.bmp", xPosition, 159 * sdlConf->yResize, 2);
  }
}


int checkRect(SetupSdl *sdlConf, int xMin, int xMax, int yMin, int Ymax){
  int test;
  test = sdlConf->event.button.x > xMin * sdlConf->xResize && sdlConf->event.button.x < xMax * sdlConf->xResize && sdlConf->event.button.y > yMin * sdlConf->yResize && sdlConf->event.button.y < Ymax * sdlConf->yResize;
  return test;
}
