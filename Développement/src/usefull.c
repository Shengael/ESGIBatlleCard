/**
 * @Author: Luis VALDEZ
 * @Date:   2017-12-11T19:38:28+01:00
 * @Email:  valdez.jds.16@gmail.com
 * @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-11T19:48:18+01:00
 * @Copyright: Shengael
 */

 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
 #include <tchar.h>


void freeThreeDimensionArrayChar(char ****array, int line, int column){
  int i, y;
  if(array == NULL) return;
  for(i = 0; i < line; i++){
    for(y = 0; y < column; y++){
      free((*array)[i][y]);
    }
    free((*array)[i]);
  }
  free(*array);
}
