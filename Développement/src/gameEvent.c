
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_rotozoom.h>
#include <string.h>
#include <tchar.h>
#include <winsock.h>
#include <time.h>
#include <MYSQL/mysql.h>
#include <database.h>
#include <display.h>
#include <game.h>
#include <gameEvent.h>

void eventInGame(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int i = 0, idCard, myTurn, loser, gameEnd;
  player->endGame = 1;
  SDL_Delay(1000);
  while(player->endGame){
    player->endTurn = 1;
    player->open = gameRoundInit(mysql, player->idGame, player->idPlayer);
    if(player->open > 10) player->open = 10;
    updateOpenRemaining(mysql, player->open, player->idGame, player->idPlayer);
    updateBoard(mysql, sdlConf, player);
    myTurn = player->open;
    while(player->endTurn){
      SDL_WaitEvent(&sdlConf->event);
      if(myTurn){
        gameplayEvent(mysql, sdlConf, player);
        globalEvent(mysql, sdlConf, player);
        if((loser = endGame(mysql, player->idGame, player->idPlayer)) != -1){
          printf("\ntestest\n");
          fflush(stdout);
          finishGame(mysql, player->idGame, loser);
          printf("testest2\n\n");
          fflush(stdout);
          player->endTurn = 0;
          player->endGame = 0;
          player->eventStatus = 6;
          printf("loser %d player %d", loser, player->idPlayer);
          fflush(stdout);
          if(loser == player->idPlayer) player->win = 0;
          else if(loser != 0) player->win = 2;
          else player->win = 1;
        }
        printf("isMyTurn\n");
        fflush(stdout);
      }
      else{
        updateBoard(mysql, sdlConf, player);
        if((gameEnd = gameIsEnd(mysql, player->idGame)) != -1){
          player->endTurn = 0;
          player->endGame = 0;
          player->eventStatus = 6;
          if(gameEnd == player->idPlayer) player->win = 2;
          else if(gameEnd != 0) player->win = 0;
          else player->win = 1;
        }
        printf("isNotMyTurn\n");
        fflush(stdout);
        if(itIsMyTurn(mysql, player->idGame, player->idPlayer)) player->endTurn = 0;
      }
      eventDisplay(sdlConf, player);
    }
  }
}

void globalEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  switch(sdlConf->event.type)
  {
    case SDL_MOUSEBUTTONDOWN:
    if(sdlConf->event.button.button == SDL_BUTTON_LEFT){
      if(sdlConf->event.button.x > 874 * sdlConf->xResize && sdlConf->event.button.x < 1073 * sdlConf->xResize && sdlConf->event.button.y > 523 * sdlConf->yResize && sdlConf->event.button.y < 593 * sdlConf->yResize){
        changeTurn(mysql, player->idGame, player->idPlayer);
        player->endTurn = 0;
        updateBoard(mysql, sdlConf, player);
      }
    }
    break;
  }
}


void gameplayEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player){
  int i, idCard = -1;
  switch(sdlConf->event.type)
  {
    case SDL_MOUSEBUTTONDOWN:
    playCardInit(sdlConf, player);
    attackInit(sdlConf, player);
    break;

    case SDL_MOUSEBUTTONUP:
    if(player->lastPlay == 0) return;
    if(player->lastPlay == 1) idCard = returnIdCard(mysql, player->currentIndex, "HAND", player->idGame,player->idPlayer);
    else if(player->lastPlay == 2) idCard = returnIdCard(mysql, player->currentIndex, "BOARD", player->idGame,player->idPlayer);
    printf("idCard %d\n", idCard);
    fflush(stdout);
    if(idCard == -1){
      player->lastPlay = 0;
      return;
    }

    if(player->lastPlay == 1) playCardEnd(mysql, sdlConf, player, idCard);
    else if(player->lastPlay == 2) attackEnd(mysql, sdlConf, player, idCard);
    break;
  }
}

void attackInit(SetupSdl *sdlConf, playerData *player){
  int i;
  if(sdlConf->event.button.button == SDL_BUTTON_LEFT && sdlConf->event.motion.y >= sdlConf->yBoard[0] && sdlConf->event.motion.y <= sdlConf->yBoard[1]){
    for(i = 0; i < 12; i += 2){
      if(sdlConf->event.motion.x >= sdlConf->xBoard[i] && sdlConf->event.motion.x <= sdlConf->xBoard[i + 1]){
        player->lastPlay = 2;
        player->currentIndex = i / 2;
        break;
      }
    }
  }
}

void attackEnd(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard){
  int i, idTarget = -1;

  if(!(sdlConf->event.button.button == SDL_BUTTON_LEFT)) return;

  if(sdlConf->event.motion.x >= 1700 * sdlConf->xResize && sdlConf->event.motion.x <= 1912 * sdlConf->xResize && sdlConf->event.motion.y >= 316 * sdlConf->yResize && sdlConf->event.motion.y <= 506 * sdlConf->yResize){
    attack(mysql, idCard, 0, player->idGame, player->idPlayer);
    player->lastPlay = 0;
    updateBoard(mysql, sdlConf, player);
    return;
  }

  if(sdlConf->event.button.button == SDL_BUTTON_LEFT && sdlConf->event.motion.y >= sdlConf->yBoardEnemy[0] && sdlConf->event.motion.y <= sdlConf->yBoardEnemy[1]){
    for(i = 0; i < 12; i += 2){
      if(sdlConf->event.motion.x >= sdlConf->xBoardEnemy[i] && sdlConf->event.motion.x <= sdlConf->xBoardEnemy[i + 1]){
        idTarget = i / 2;
        break;
      }
    }
  }

  if(idTarget != -1){
    idTarget = returnIdCard(mysql, idTarget, "BOARD", player->idGame, getIdEnemy(mysql, player->idGame, player->idPlayer));
    printf("idTarget enemy : %d\n", idTarget);
    if(idTarget == -1){
      player->lastPlay = 0;
      return;
    }
    attack(mysql, idCard, idTarget, player->idGame, player->idPlayer);
    updateBoard(mysql, sdlConf, player);
    player->lastPlay = 0;
    return;
  }
}


void playCardInit(SetupSdl *sdlConf, playerData *player){
  int i;
  if(sdlConf->event.button.button == SDL_BUTTON_LEFT && (sdlConf->event.motion.y >= sdlConf->yHand[0] && sdlConf->event.motion.y <= sdlConf->yHand[1])){
    for(i = 0; i < 16; i += 2){
      if(sdlConf->event.motion.x >= sdlConf->xHand[i] && sdlConf->event.motion.x <= sdlConf->xHand[i + 1]){
        player->lastPlay = 1;
        player->currentIndex = i / 2;
        break;
      }
    }
  }
}

void playCardEnd(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard){
  if(!(sdlConf->event.button.button == SDL_BUTTON_LEFT)) return;
  if(!isServant(mysql, idCard)) printf("cat spell %d\n", spellCat(mysql, idCard));
  if(isServant(mysql, idCard)){
    printf("isServant\n");
    fflush(stdout);
    PLAYServantEvent(mysql, sdlConf, player, idCard);
  }
  else if(spellCat(mysql, idCard) == DRAWCARD){
    printf("isDraw\n");
    fflush(stdout);
    playResearch(mysql, sdlConf, player, idCard);
  }
  else{
    printf("isSpell\n");
    fflush(stdout);
    playSpellEvent(mysql, sdlConf, player, idCard);
  }
  printf("end\n");
  fflush(stdout);
  player->lastPlay = 0;
  updateBoard(mysql, sdlConf, player);
}


void PLAYServantEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard){
  printf("serviteur\n");
  fflush(stdout);
  if(sdlConf->event.button.button == SDL_BUTTON_LEFT && (sdlConf->event.motion.y >= sdlConf->yBoard[0] && sdlConf->event.motion.y <= sdlConf->yBoard[1])){
    printf("serviteur joué\n");
    fflush(stdout);
    playCard(mysql, player->idGame, player->idPlayer, idCard, 0, &player->open);
    printf("serviteur joué V2\n");
    fflush(stdout);
  }
  player->lastPlay = 0;
}


void playResearch(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard){
  if(sdlConf->event.button.button == SDL_BUTTON_LEFT && sdlConf->event.motion.y <= sdlConf->yHand[0]){
    printf("pioche\n");
    fflush(stdout);
    playCard(mysql, player->idGame, player->idPlayer, idCard, 0, &player->open);
  }
  else player->lastPlay = 0;
}


void playSpellEvent(MYSQL *mysql, SetupSdl *sdlConf, playerData *player, int idCard){
  int idTarget = -1, i;
  if(sdlConf->event.button.button != SDL_BUTTON_LEFT) return;

  if(sdlConf->event.motion.y >= sdlConf->yBoardEnemy[0] && sdlConf->event.motion.y <= sdlConf->yBoardEnemy[1]){
    for(i = 0; i < 12; i += 2){
      if(sdlConf->event.motion.x >= sdlConf->xBoardEnemy[i] && sdlConf->event.motion.x <= sdlConf->xBoardEnemy[i + 1]){
        idTarget = i / 2;
        break;
      }
    }

    if(idTarget != -1){
      idTarget = returnIdCard(mysql, idTarget, "BOARD", player->idGame, getIdEnemy(mysql, player->idGame, player->idPlayer));
      printf("idTarget enemy : %d\n", idTarget);
      if(idTarget == -1){
        player->lastPlay = 0;
        return;
      }
      playCard(mysql, player->idGame, player->idPlayer, idCard, idTarget, &player->open);
      return;
    }
  }

  if(sdlConf->event.motion.y >= sdlConf->yBoard[0] && sdlConf->event.motion.y <= sdlConf->yBoard[1]){
    for(i = 0; i < 12; i += 2){
      if(sdlConf->event.motion.x >= sdlConf->xBoard[i] && sdlConf->event.motion.x <= sdlConf->xBoard[i + 1]){
        idTarget = i / 2;
        break;
      }
    }

    if(idTarget != -1){
      idTarget = returnIdCard(mysql, idTarget, "BOARD", player->idGame, player->idPlayer);
      printf("idTarget allié : %d\n", idTarget);
      if(idTarget == -1){
        player->lastPlay = 0;
        return;
      }
      playCard(mysql, player->idGame, player->idPlayer, idCard, idTarget, &player->open);
      return;
    }
  }

  player->lastPlay = 0;
}
