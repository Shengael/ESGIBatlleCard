/**
* @Author: Luis VALDEZ <Shengael>
* @Date:   2017-12-07T21:51:26+01:00
* @Email:  valdez.jds.16@gmail.com
* @Project: ESGIBATTLECARD
 * @Last modified by:   Luis VALDEZ
 * @Last modified time: 2017-12-18T12:03:58+01:00
* @Copyright: Shengael
*/



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <winsock.h>
#include "MYSQL/mysql.h"
#include "enum.h"
#include "database.h"
#include "SDL/SDL.h"
#include "display.h"
int main(int argc, char *argv[])
{
  srand(time(NULL));
  SetupSdl *sdlConf = initDisplay();
  playerData player;
  MYSQL *mysql = connectDb();
  player.idPlayer = (rand() % 46) + 2;
  player.eventStatus = 0;
  player.lastPlay = 0;
  player.currentIndex = 0;
  player.pseudo = malloc(25);
  player.pseudo[0] = '\0';
  player.passWord = malloc(25);
  player.passWord[0] = '\0';
  player.vPassword = malloc(25);
  player.vPassword[0] = '\0';
  eventManagement(mysql, sdlConf, &player);

  TTF_CloseFont(sdlConf->police);
  TTF_CloseFont(sdlConf->policeDeck);
  TTF_CloseFont(sdlConf->policeText);
  free(sdlConf);
  free(player.vPassword);
  free(player.pseudo);
  free(player.passWord);
  SDL_Quit();
}
