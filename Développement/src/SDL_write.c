#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_rotozoom.h>
#include <string.h>
#include <tchar.h>
#include <winsock.h>
#include <time.h>
#include <MYSQL/mysql.h>
#include <database.h>
#include <display.h>
#include <game.h>
#include <gameEvent.h>



void displayText(char *text, SetupSdl *sdlConf, int x, int y, int indice){
  SDL_Rect position;
  SDL_Surface *textSurface;


  switch (indice) {
    case 0:
    textSurface = TTF_RenderText_Blended(sdlConf->policeText, text, sdlConf->error);
    break;
    case 1:
    textSurface = TTF_RenderText_Blended(sdlConf->policeText, text, sdlConf->color);
    break;
    case 2:
    textSurface = TTF_RenderText_Blended(sdlConf->police, text, sdlConf->color);
    break;
    case 3:
    textSurface = TTF_RenderText_Blended(sdlConf->policeDeck, text, sdlConf->color);
    break;

  }

  position.x = x;
  position.y = y;
  SDL_BlitSurface(textSurface, NULL, sdlConf->screen, &position);
  SDL_FreeSurface(textSurface);
}


void inputData(char **text, SetupSdl *sdlConf, int sizeOfInput){
  char inputKey;
  int currentIndex;

  switch(sdlConf->event.type)
  {
    case SDL_KEYDOWN:
    currentIndex = strlen(*text);
    printf("imput touche %c\n", sdlConf->event.key.keysym.unicode);
    fflush(stdout);
    inputKey = sdlConf->event.key.keysym.unicode;
    if((inputKey > 64 && inputKey < 91) || (inputKey > 96 && inputKey < 123) || (inputKey > 47 && inputKey < 58)){
      if(currentIndex < sizeOfInput - 2){
        (*text)[currentIndex] = inputKey;
        (*text)[currentIndex + 1] = '\0';
      }
    }
    else if(inputKey == 8) (*text)[--currentIndex] = '\0';
    break;
  }

}
