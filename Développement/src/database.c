/**
* @Author: Luis VALDEZ
* @Date:   2017-12-07T21:57:25+01:00
* @Email:  valdez.jds.16@gmail.com
* @Project: ESGIBATTLECARD
* @Last modified by:   Luis VALDEZ
* @Last modified time: 2017-12-18T12:28:07+01:00
* @Copyright: Shengael
*/



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <errno.h>
#include <winsock.h>
#include "MYSQL/mysql.h"
#include "database.h"
#include "enum.h"
#include "usefull.h"

/**
* Connexion à une base de données
* @return la connexion MYSQL
*/
MYSQL *connectDb(){
  MYSQL *mysql;
  mysql = mysql_init(NULL);
  mysql_options(mysql, MYSQL_READ_DEFAULT_GROUP, "option");
  if(mysql_real_connect(mysql, "www.libart.xyz", "esgibattlecard", "esgi2017", "ESGIBATTLECARD", 0, NULL, 0)) {
    return mysql;
  }

  return NULL;
}

/**
* récupére le résultat d'un requète dans un tableau à 2 dimensions, avec le nombre de ligne et le nombre de colonne
* @param mysql  connexion MYSQL
* @param query  chaine contenant la requete MYSQL
* @param string le tableau dans lequel nous obtiendront le résultat
* @param line   la variable qui contiendra le nombre de ligne
* @param column la variable qui contiendra le nombre de colonne
*/
void dataPull(MYSQL *mysql, char *query, char ****string, int *line, int *column)
{
  //Déclaration des variables
  char ***request = NULL;
  int cpt = 0;
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  unsigned int i = 0;
  unsigned int numChamps = 0;
  unsigned long *lengths;
  if(*string != NULL){
    freeThreeDimensionArrayChar(string, *line, *column);
    *string = NULL;
  }
  //Requète
  mysql_query(mysql, query);
  //On met le jeu de résultat dans le pointeur result
  result = mysql_use_result(mysql);
  //On récupère le nombre de champs
  numChamps = mysql_num_fields(result);
  //Tant qu'il y a encore un résultat ...
  while ((row = mysql_fetch_row(result)))
  {
    request = addLineResult(&request, numChamps, cpt);
    if(request == NULL){
      *string = request;
      return;
    }
    //On fait une boucle pour avoir la valeur de chaque champs
    for(i = 0; i < numChamps; i++){
      if(row[i] == NULL) request[cpt][i] = NULL;
      else strcpy(request[cpt][i], row[i]);
    }
    cpt++;
  }

  *line = cpt;
  *column = numChamps;
  *string = request;
  //Libération du jeu de résultat
  mysql_free_result(result);
}

/**
* Permet d'ajouter une ligne à notre tableau à deux dimensions
* @param  request   ancien tableau
* @param  numChamps quantité de colonne dans l'ancien tableau
* @param  line      quantité de ligne dans l'ancien tableau
* @return           retourne le nouveau tableau avec une nouvelle ligne vide
*/
char ***addLineResult(char ****request, int numChamps, int line){
  int y, z;
  //allocation tableau temporaire nombre de ligne +1
  char ***temp = malloc(sizeof(char **) * (line + 1));
  if(temp == NULL) return temp;
  //allocation total du tableau temporaire plus affectation de la valeur 0
  for(y = 0; y < line + 1; y++){
    temp[y] = malloc(sizeof(char *) * numChamps);
    if(temp[y] == NULL) return NULL;
    for(z = 0; z < numChamps; z++){
      temp[y][z] = malloc(sizeof(char) * 51);
      memset(temp[y][z], '\0', 51);
      //copie du tableau

      if(y != line){
        if((*request)[y][z] == NULL) temp[y][z] = NULL;
        else{
          strcpy(temp[y][z], (*request)[y][z]);
        }
      }
    }
  }
  freeThreeDimensionArrayChar(request, line, numChamps);
  return temp;
}
