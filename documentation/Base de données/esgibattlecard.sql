-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 18 Décembre 2017 à 16:59
-- Version du serveur :  5.5.55-0+deb8u1
-- Version de PHP :  5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `esgibattlecard`
--

-- --------------------------------------------------------

--
-- Structure de la table `ask`
--

CREATE TABLE IF NOT EXISTS `ask` (
  `idPlayer` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `lifeModified` int(11) DEFAULT NULL,
  `attackModified` int(11) DEFAULT NULL,
  `sleep` tinyint(1) NOT NULL,
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `name` varchar(25) DEFAULT NULL,
`cardId` int(11) NOT NULL,
  `deck` int(11) DEFAULT NULL,
  `spell` int(11) DEFAULT NULL,
  `life` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `openCost` int(11) NOT NULL,
  `specialEffect` int(11) DEFAULT NULL,
  `spellEffect` int(11) DEFAULT NULL,
  `tinyPicture` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `card`
--

INSERT INTO `card` (`name`, `cardId`, `deck`, `spell`, `life`, `attack`, `openCost`, `specialEffect`, `spellEffect`, `tinyPicture`, `picture`) VALUES
('Mason', 1, 0, NULL, 4, 9, 1, 1, NULL, 'NAK14QBA5LD', 'HAC70KMG4KQ'),
('Galvin', 2, 0, NULL, 9, 0, 9, 1, NULL, 'ZWB37ZLX7TX', 'PSN81MIU5XV'),
('Philip', 3, 0, NULL, 6, 9, 3, 3, NULL, 'CZY61EZY6CN', 'BWR17VSY9XS'),
('Bell', 4, 0, NULL, 2, 9, 5, 2, NULL, 'XFN11CNI0RQ', 'JYB46IRO6JK'),
('Eagan', 5, 0, NULL, 1, 4, 4, 2, NULL, 'AJF66OGP5EU', 'IUH84BJX1UA'),
('Stewart', 6, 0, NULL, 5, 8, 10, 1, NULL, 'RLH16KUU0NH', 'NFB30LDQ2MT'),
('Lane', 7, 0, NULL, 4, 3, 6, 3, NULL, 'RFO99BKF2VZ', 'VYR75CEO1XL'),
('Germane', 8, 0, NULL, 6, 1, 8, 3, NULL, 'VXV63VBC2LU', 'PQX54KZY7RH'),
('Cara', 9, 0, NULL, 4, 7, 7, 2, NULL, 'MHS20JKI3OG', 'EAH60GZU2NY'),
('Rhona', 10, 0, NULL, 8, 3, 7, 2, NULL, 'WPF64JRT0NU', 'HOE30UPC7JZ'),
('Cruz', 11, 0, NULL, 2, 8, 7, 1, NULL, 'IRG82QNY6JO', 'HJX89XIO2EG'),
('Garrison', 12, 0, 2, NULL, NULL, 0, NULL, 2, 'PRJ59AHI3KH', 'RIC27EKZ8ZU'),
('Victor', 13, 0, 1, NULL, NULL, 10, NULL, 3, 'KWC86UOF0XM', 'OEP93ZRA3VH'),
('Janna', 14, 0, 1, NULL, NULL, 8, NULL, 5, 'QHQ97WRG9RZ', 'PWB79BBH9JJ'),
('Dakota', 15, 0, 3, NULL, NULL, 1, NULL, 5, 'BCP51KEP9SP', 'OPZ23STQ7VZ'),
('Frances', 16, 0, NULL, 6, 1, 3, 1, NULL, 'FSI87XVP7BH', 'XUX50TGM5YY'),
('Jessica', 17, 0, NULL, 5, 7, 9, 2, NULL, 'FWT97LEV6AC', 'KGB73ZSA0YB'),
('Jason', 18, 0, NULL, 10, 7, 8, 1, NULL, 'CEF17HIS0VF', 'WZB14IRV0JW'),
('Rae', 19, 0, NULL, 1, 3, 0, 3, NULL, 'WFU92WOA7QA', 'SHK25KTK4VG'),
('Lars', 20, 0, NULL, 9, 9, 0, 2, NULL, 'ZMW93OMF1LK', 'JUC27TEG8UW'),
('Carissa', 21, 0, NULL, 8, 0, 10, 3, NULL, 'GJB97UGR9EU', 'RYU28JKD1SA'),
('Regina', 22, 0, 1, NULL, NULL, 6, NULL, 3, 'PIV33UKF3XN', 'GLS15ZHL1PD'),
('Jakeem', 23, 0, 3, NULL, NULL, 3, NULL, 5, 'RNC17JMW9CK', 'CDO21KAS3DL'),
('Joan', 24, 0, 3, NULL, NULL, 4, NULL, 3, 'RQU13VLG9MG', 'SMM21XBP0ZT'),
('Jonas', 25, 0, 2, NULL, NULL, 10, NULL, 3, 'RMK17FGQ9LC', 'JDO50HHZ7UM'),
('Devin', 26, 0, 0, NULL, NULL, 7, NULL, 5, 'KUR32FSL3ME', 'BXX10CKB1MF'),
('Noelle', 27, 0, 0, NULL, NULL, 6, NULL, 4, 'ONN04IYX2DJ', 'GKX81LXY8BY'),
('Galena', 28, 0, NULL, 8, 7, 3, 1, NULL, 'UWV59QEZ9KA', 'HUZ05VFU1VM'),
('Rinah', 29, 0, NULL, 1, 1, 0, 3, NULL, 'DDL67ZCN4IZ', 'ZFT10FFB1YI'),
('Imogene', 30, 0, NULL, 4, 0, 9, 3, NULL, 'PHA72INL4KV', 'CHZ74AMN1RG'),
('Zane', 31, 1, NULL, 4, 6, 9, 2, NULL, 'HNQ39VRD2AA', 'ZVO44FYZ3YM'),
('Zachery', 32, 1, NULL, 1, 8, 2, 1, NULL, 'HGN94BRQ9LQ', 'VQT47ATW1LA'),
('Alden', 33, 1, NULL, 9, 2, 2, 2, NULL, 'GQM81AJM7PW', 'BJZ20DIS1EV'),
('Fallon', 34, 1, NULL, 1, 10, 2, 2, NULL, 'UEW61NMP2BC', 'WOY18TNQ0WZ'),
('Athena', 35, 1, NULL, 4, 6, 9, 1, NULL, 'QJH54UVZ8YK', 'YMX86UIQ2UT'),
('Heather', 36, 1, NULL, 2, 7, 6, 1, NULL, 'MDI26BFS2OV', 'MHM35KPD3SX'),
('Seth', 37, 1, NULL, 2, 7, 3, 3, NULL, 'NZT41TNF3DZ', 'CSY13USQ6IE'),
('Deacon', 38, 1, NULL, 9, 7, 3, 3, NULL, 'TRF86GDM5NF', 'HCV93PZT3UC'),
('Cheyenne', 39, 1, NULL, 8, 7, 1, 2, NULL, 'CQI59ZAN4SX', 'ECA26GQM5NB'),
('Lance', 40, 1, NULL, 2, 1, 1, 2, NULL, 'AZL87AXY3VX', 'XVN99SGN5AD'),
('Deanna', 41, 1, NULL, 1, 8, 9, 2, NULL, 'PVS92QKG8IL', 'EQD80VUS2DX'),
('Lillith', 42, 1, NULL, 9, 3, 10, 1, NULL, 'YGF97QPP0RF', 'UFC48DVD3WK'),
('Emma', 43, 1, NULL, 3, 2, 6, 3, NULL, 'FHP10QJI5KA', 'VAQ76GHF0EK'),
('Bryar', 44, 1, NULL, 1, 5, 3, 2, NULL, 'TGD42KCR2IV', 'JYU79FED6WO'),
('Lana', 45, 1, NULL, 10, 7, 3, 1, NULL, 'BPR48MXM8YQ', 'DUF66EGG7RC'),
('Jamalia', 46, 1, NULL, 10, 2, 5, 1, NULL, 'XQT26FSO3QY', 'FSF09TYU9GR'),
('Raymond', 47, 1, NULL, 4, 8, 7, 3, NULL, 'ZAH05HKD8YM', 'HEY23ROZ0NA'),
('Larissa', 48, 1, NULL, 1, 3, 4, 2, NULL, 'PXE95ZQS6JY', 'WGS67GSY0US'),
('Avram', 49, 1, NULL, 6, 7, 6, 3, NULL, 'HSA53HTM5EU', 'UZS70DDI1BE'),
('Reagan', 50, 1, NULL, 8, 1, 0, 2, NULL, 'ERS84CFG2KJ', 'WQN06HNZ9FS'),
('Nola', 51, 1, 1, NULL, NULL, 4, NULL, 2, 'DIP32IIX5QV', 'EYG40IXZ8KP'),
('Kenneth', 52, 1, 1, NULL, NULL, 8, NULL, 3, 'QKA99BZJ5WT', 'UQD84YDE9GJ'),
('Dominic', 53, 1, 2, NULL, NULL, 2, NULL, 5, 'ZTI80FBG7VM', 'RSF48AEB3AX'),
('Michelle', 54, 1, 3, NULL, NULL, 1, NULL, 5, 'RKG85IGV0CK', 'DSD49TAJ9US'),
('Jasper', 55, 1, 1, NULL, NULL, 0, NULL, 5, 'NQU75AXC3BM', 'VDD28YSY5IW'),
('Carter', 56, 1, 0, NULL, NULL, 2, NULL, 4, 'DFQ34YAT2GY', 'ZQF09OHL6SR'),
('Warren', 57, 1, 2, NULL, NULL, 9, NULL, 2, 'XOD69HYC9QU', 'CNE94VXC1QR'),
('MacKensie', 58, 1, 0, NULL, NULL, 0, NULL, 5, 'WYK00LVH8UV', 'CKP35AXT9YJ'),
('Caldwell', 59, 1, 3, NULL, NULL, 9, NULL, 1, 'DVG44FDO0JG', 'PMV22KGA3YP'),
('Rhiannon', 60, 1, 1, NULL, NULL, 9, NULL, 5, 'KDV02OEQ0SU', 'PKN13SWV5NI');

-- --------------------------------------------------------

--
-- Structure de la table `deck`
--

CREATE TABLE IF NOT EXISTS `deck` (
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `deck`
--

INSERT INTO `deck` (`cardId`, `matchId`, `idPlayer`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1),
(4, 1, 1),
(5, 1, 1),
(6, 1, 1),
(7, 1, 1),
(8, 1, 1),
(9, 1, 1),
(10, 1, 1),
(11, 1, 1),
(12, 1, 1),
(13, 1, 1),
(14, 1, 1),
(15, 1, 1),
(16, 1, 1),
(17, 1, 1),
(18, 1, 1),
(19, 1, 1),
(20, 1, 1),
(21, 1, 1),
(22, 1, 1),
(23, 1, 1),
(24, 1, 1),
(25, 1, 1),
(26, 1, 1),
(27, 1, 1),
(28, 1, 1),
(29, 1, 1),
(30, 1, 1),
(31, 1, 21),
(32, 1, 21),
(33, 1, 21),
(34, 1, 21),
(35, 1, 21),
(36, 1, 21),
(37, 1, 21),
(38, 1, 21),
(39, 1, 21),
(40, 1, 21),
(41, 1, 21),
(42, 1, 21),
(43, 1, 21),
(44, 1, 21),
(45, 1, 21),
(46, 1, 21),
(47, 1, 21),
(48, 1, 21),
(49, 1, 21),
(50, 1, 21),
(51, 1, 21),
(52, 1, 21),
(53, 1, 21),
(54, 1, 21),
(55, 1, 21),
(56, 1, 21),
(57, 1, 21),
(58, 1, 21),
(59, 1, 21),
(60, 1, 21),
(1, 2, 2),
(2, 2, 2),
(3, 2, 2),
(4, 2, 2),
(5, 2, 2),
(6, 2, 2),
(7, 2, 2),
(8, 2, 2),
(9, 2, 2),
(10, 2, 2),
(11, 2, 2),
(12, 2, 2),
(13, 2, 2),
(14, 2, 2),
(15, 2, 2),
(16, 2, 2),
(17, 2, 2),
(18, 2, 2),
(19, 2, 2),
(20, 2, 2),
(21, 2, 2),
(22, 2, 2),
(23, 2, 2),
(24, 2, 2),
(25, 2, 2),
(26, 2, 2),
(27, 2, 2),
(28, 2, 2),
(29, 2, 2),
(30, 2, 2),
(31, 2, 22),
(32, 2, 22),
(33, 2, 22),
(34, 2, 22),
(35, 2, 22),
(36, 2, 22),
(37, 2, 22),
(38, 2, 22),
(39, 2, 22),
(40, 2, 22),
(41, 2, 22),
(42, 2, 22),
(43, 2, 22),
(44, 2, 22),
(45, 2, 22),
(46, 2, 22),
(47, 2, 22),
(48, 2, 22),
(49, 2, 22),
(50, 2, 22),
(51, 2, 22),
(52, 2, 22),
(53, 2, 22),
(54, 2, 22),
(55, 2, 22),
(56, 2, 22),
(57, 2, 22),
(58, 2, 22),
(59, 2, 22),
(60, 2, 22),
(1, 3, 3),
(2, 3, 3),
(3, 3, 3),
(4, 3, 3),
(5, 3, 3),
(6, 3, 3),
(7, 3, 3),
(8, 3, 3),
(9, 3, 3),
(10, 3, 3),
(11, 3, 3),
(12, 3, 3),
(13, 3, 3),
(14, 3, 3),
(15, 3, 3),
(16, 3, 3),
(17, 3, 3),
(18, 3, 3),
(19, 3, 3),
(20, 3, 3),
(21, 3, 3),
(22, 3, 3),
(23, 3, 3),
(24, 3, 3),
(25, 3, 3),
(26, 3, 3),
(27, 3, 3),
(28, 3, 3),
(29, 3, 3),
(30, 3, 3),
(31, 3, 23),
(32, 3, 23),
(33, 3, 23),
(34, 3, 23),
(35, 3, 23),
(36, 3, 23),
(37, 3, 23),
(38, 3, 23),
(39, 3, 23),
(40, 3, 23),
(41, 3, 23),
(42, 3, 23),
(43, 3, 23),
(44, 3, 23),
(45, 3, 23),
(46, 3, 23),
(47, 3, 23),
(48, 3, 23),
(49, 3, 23),
(50, 3, 23),
(51, 3, 23),
(52, 3, 23),
(53, 3, 23),
(54, 3, 23),
(55, 3, 23),
(56, 3, 23),
(57, 3, 23),
(58, 3, 23),
(59, 3, 23),
(60, 3, 23);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
`matchId` int(11) NOT NULL,
  `turn` int(11) DEFAULT NULL,
  `firstPlayer` int(11) DEFAULT NULL,
  `round` int(11) NOT NULL DEFAULT '1',
  `save` tinyint(1) NOT NULL DEFAULT '0',
  `winnerId` int(11) DEFAULT NULL,
  `looserId` int(11) DEFAULT NULL,
  `tournamentId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `game`
--

INSERT INTO `game` (`matchId`, `turn`, `firstPlayer`, `round`, `save`, `winnerId`, `looserId`, `tournamentId`) VALUES
(1, NULL, NULL, 8, 0, NULL, NULL, NULL),
(2, NULL, NULL, 4, 1, NULL, NULL, NULL),
(3, NULL, NULL, 6, 1, NULL, NULL, NULL),
(4, NULL, NULL, 2, 0, NULL, NULL, NULL),
(5, NULL, NULL, 7, 1, NULL, NULL, NULL),
(6, NULL, NULL, 7, 0, NULL, NULL, NULL),
(7, NULL, NULL, 7, 0, NULL, NULL, NULL),
(8, NULL, NULL, 2, 1, NULL, NULL, NULL),
(9, NULL, NULL, 2, 0, NULL, NULL, NULL),
(10, NULL, NULL, 8, 1, NULL, NULL, NULL),
(11, NULL, NULL, 8, 0, NULL, NULL, NULL),
(12, NULL, NULL, 9, 0, NULL, NULL, NULL),
(13, NULL, NULL, 8, 1, NULL, NULL, NULL),
(14, NULL, NULL, 10, 0, NULL, NULL, NULL),
(15, NULL, NULL, 3, 0, NULL, NULL, NULL),
(16, NULL, NULL, 5, 0, NULL, NULL, NULL),
(17, NULL, NULL, 3, 1, NULL, NULL, NULL),
(18, NULL, NULL, 10, 1, NULL, NULL, NULL),
(19, NULL, NULL, 1, 1, NULL, NULL, NULL),
(20, NULL, NULL, 3, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `hand`
--

CREATE TABLE IF NOT EXISTS `hand` (
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

CREATE TABLE IF NOT EXISTS `participe` (
  `satus` int(11) NOT NULL,
  `turnOut` int(11) DEFAULT NULL,
  `idPlayer` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `play`
--

CREATE TABLE IF NOT EXISTS `play` (
`idPlay` int(11) NOT NULL,
  `playerLife` int(11) NOT NULL DEFAULT '35',
  `idPlayer` int(11) NOT NULL,
  `matchId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `play`
--

INSERT INTO `play` (`idPlay`, `playerLife`, `idPlayer`, `matchId`) VALUES
(1, 18, 1, 1),
(2, 23, 2, 2),
(3, 28, 3, 3),
(4, 18, 21, 1),
(5, 23, 22, 2),
(6, 28, 23, 3);

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
`idPlayer` int(11) NOT NULL,
  `nickname` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `win` int(11) NOT NULL DEFAULT '0',
  `loose` int(11) NOT NULL DEFAULT '0',
  `currentTournament` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `player`
--

INSERT INTO `player` (`idPlayer`, `nickname`, `email`, `password`, `token`, `avatar`, `win`, `loose`, `currentTournament`) VALUES
(1, 'IA', 'libart@libart.xyz', 'esgi2017', NULL, NULL, 100, 2, NULL),
(2, 'Owen', 'lobortis@vel.co.uk', 'ADP41VBA3MK', NULL, NULL, 24, 68, NULL),
(3, 'Cheryl', 'lorem@neque.org', 'ABX87GDI1GT', NULL, NULL, 60, 5, NULL),
(4, 'Jin', 'elementum.purus.accumsan@nisl.co.uk', 'BAX75SLU9NT', NULL, NULL, 85, 89, NULL),
(5, 'Kamal', 'nulla.Integer@accumsansed.net', 'EJB31XFH7LM', NULL, NULL, 11, 31, NULL),
(6, 'Ashely', 'orci@porttitor.org', 'YBW20GCK9GK', NULL, NULL, 17, 5, NULL),
(7, 'Gloria', 'facilisis@quamPellentesquehabitant.net', 'RMS73XSS1MA', NULL, NULL, 38, 36, NULL),
(8, 'Isaac', 'eu.neque.pellentesque@non.org', 'WYU74NCT8SG', NULL, NULL, 21, 92, NULL),
(9, 'Donna', 'eu.eleifend.nec@nuncacmattis.com', 'XPM20YQY2VI', NULL, NULL, 47, 41, NULL),
(10, 'Abbot', 'sem@netus.ca', 'YKD58ODE9TI', NULL, NULL, 95, 77, NULL),
(11, 'Jolie', 'aliquam.eu@interdumSedauctor.net', 'OHH69MWH5UR', NULL, NULL, 85, 62, NULL),
(12, 'Sylvia', 'Fusce.feugiat@volutpat.edu', 'IIJ57MER9XW', NULL, NULL, 86, 54, NULL),
(13, 'Vance', 'Proin@odio.co.uk', 'UIG15GDR1GC', NULL, NULL, 48, 73, NULL),
(14, 'TaShya', 'Cras.dictum@placeratCrasdictum.edu', 'GPH72DPG2CQ', NULL, NULL, 75, 21, NULL),
(15, 'Mia', 'nisl.Quisque.fringilla@magnaSuspendissetristique.c', 'TGJ97XYB1YY', NULL, NULL, 9, 54, NULL),
(16, 'Karly', 'lacus.Aliquam.rutrum@fames.com', 'KYY69LYS5GZ', NULL, NULL, 67, 77, NULL),
(17, 'Cain', 'tristique@semperauctor.org', 'XBA09NNK4LW', NULL, NULL, 88, 99, NULL),
(18, 'Mufutau', 'consequat.lectus.sit@semperegestasurna.ca', 'HSZ95AHC1IN', NULL, NULL, 37, 61, NULL),
(19, 'Gavin', 'in@vulputateeu.org', 'PCK11KCH5WK', NULL, NULL, 57, 7, NULL),
(20, 'Claudia', 'a.nunc.In@Namac.org', 'SOB37PBO6VG', NULL, NULL, 52, 3, NULL),
(21, 'Gemma', 'Suspendisse.sagittis.Nullam@maurisSuspendissealiqu', 'QLK71JFM8BM', NULL, NULL, 27, 14, NULL),
(22, 'Clarke', 'turpis@egetvolutpatornare.ca', 'GPO13GSV7QZ', NULL, NULL, 45, 77, NULL),
(23, 'Channing', 'nulla@utnisi.net', 'PEI12EXG7PT', NULL, NULL, 23, 57, NULL),
(24, 'Hunter', 'nisl.arcu@purusaccumsaninterdum.edu', 'RYU66SUU9VN', NULL, NULL, 6, 23, NULL),
(25, 'Ferdinand', 'Fusce.mollis.Duis@fringillaest.net', 'OOA88TMG3JT', NULL, NULL, 39, 88, NULL),
(26, 'Quynn', 'semper.rutrum@mattis.net', 'BEY66UUX3JT', NULL, NULL, 49, 6, NULL),
(27, 'Brenda', 'venenatis@Maurismagna.co.uk', 'ZLE78LWW2QC', NULL, NULL, 100, 72, NULL),
(28, 'Vanna', 'sed.hendrerit@pedeSuspendissedui.org', 'TXY63LFB7LC', NULL, NULL, 7, 78, NULL),
(29, 'Alec', 'dapibus@dolorelit.net', 'NLF70LMS7AD', NULL, NULL, 45, 61, NULL),
(30, 'Davis', 'ac.nulla.In@tempor.ca', 'OON97LHZ9CN', NULL, NULL, 58, 85, NULL),
(31, 'Byron', 'tempus.scelerisque.lorem@velvenenatis.ca', 'SKE26XOA8XT', NULL, NULL, 99, 95, NULL),
(32, 'Athena', 'pretium.et.rutrum@Crasdictumultricies.co.uk', 'TLV38UAM6GZ', NULL, NULL, 39, 69, NULL),
(33, 'Lara', 'eu@Quisque.org', 'SJR22PVW1PD', NULL, NULL, 94, 46, NULL),
(34, 'Akeem', 'Nunc@egestasa.net', 'XYV45WOL1FB', NULL, NULL, 21, 31, NULL),
(35, 'Brendan', 'quis.arcu.vel@posuerecubiliaCurae.org', 'AHT38CEG3KL', NULL, NULL, 44, 94, NULL),
(36, 'Chiquita', 'nunc.sit.amet@suscipit.edu', 'TRE70OJU3EU', NULL, NULL, 61, 78, NULL),
(37, 'Jerry', 'dis@gravidasitamet.net', 'YLA39RKH3RZ', NULL, NULL, 47, 61, NULL),
(38, 'Tallulah', 'Sed.eu.nibh@facilisis.net', 'JVD45MNB1PB', NULL, NULL, 41, 92, NULL),
(39, 'Idona', 'lorem@arcuSedet.co.uk', 'ICK61XGX1DP', NULL, NULL, 44, 60, NULL),
(40, 'Ignatius', 'nunc.id@tempusmauriserat.com', 'TFK27WII1FL', NULL, NULL, 31, 20, NULL),
(41, 'Mikayla', 'arcu.ac@estmauris.net', 'DBA98UVQ8KW', NULL, NULL, 71, 92, NULL),
(42, 'Regan', 'Vivamus@molestiepharetra.co.uk', 'OXR64SOA2GK', NULL, NULL, 52, 15, NULL),
(43, 'Uriel', 'feugiat@aliquetnec.ca', 'GWY52VNW2LI', NULL, NULL, 61, 85, NULL),
(44, 'Ivor', 'ante.ipsum@magna.edu', 'SHQ10LPE8DL', NULL, NULL, 18, 23, NULL),
(45, 'Sierra', 'Nam.porttitor@Maurisquis.com', 'ZSK48VVD1NG', NULL, NULL, 83, 8, NULL),
(46, 'Angelica', 'suscipit.nonummy@amet.org', 'WPF25SMB8VZ', NULL, NULL, 41, 49, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `search_game`
--

CREATE TABLE IF NOT EXISTS `search_game` (
`idSearch` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL,
  `idPlayer2` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tournament`
--

CREATE TABLE IF NOT EXISTS `tournament` (
  `tournamentId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `playerNumber` int(11) DEFAULT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ask`
--
ALTER TABLE `ask`
 ADD PRIMARY KEY (`idPlayer`,`tournamentId`), ADD KEY `FK_ASK_tournamentId` (`tournamentId`);

--
-- Index pour la table `board`
--
ALTER TABLE `board`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_BOARD_matchId` (`matchId`), ADD KEY `FK_BOARD_idPlayer` (`idPlayer`);

--
-- Index pour la table `card`
--
ALTER TABLE `card`
 ADD PRIMARY KEY (`cardId`), ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `deck`
--
ALTER TABLE `deck`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_DECK_matchId` (`matchId`), ADD KEY `FK_DECK_idPlayer` (`idPlayer`);

--
-- Index pour la table `game`
--
ALTER TABLE `game`
 ADD PRIMARY KEY (`matchId`), ADD KEY `FK_GAME_tournamentId` (`tournamentId`);

--
-- Index pour la table `hand`
--
ALTER TABLE `hand`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_HAND_matchId` (`matchId`), ADD KEY `FK_HAND_idPlayer` (`idPlayer`);

--
-- Index pour la table `participe`
--
ALTER TABLE `participe`
 ADD PRIMARY KEY (`idPlayer`,`tournamentId`), ADD KEY `FK_participe_tournamentId` (`tournamentId`);

--
-- Index pour la table `play`
--
ALTER TABLE `play`
 ADD PRIMARY KEY (`idPlay`), ADD KEY `FK_PLAY_idPlayer` (`idPlayer`), ADD KEY `FK_PLAY_matchId` (`matchId`);

--
-- Index pour la table `player`
--
ALTER TABLE `player`
 ADD PRIMARY KEY (`idPlayer`), ADD UNIQUE KEY `nickname` (`nickname`,`email`,`token`);

--
-- Index pour la table `search_game`
--
ALTER TABLE `search_game`
 ADD PRIMARY KEY (`idSearch`), ADD KEY `FK_SEARCH_GAME_idPlayer_PLAYER` (`idPlayer2`), ADD KEY `FK_SEARCH_GAME_idPlayer` (`idPlayer`);

--
-- Index pour la table `tournament`
--
ALTER TABLE `tournament`
 ADD PRIMARY KEY (`tournamentId`), ADD KEY `FK_TOURNAMENT_idPlayer` (`idPlayer`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `card`
--
ALTER TABLE `card`
MODIFY `cardId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `game`
--
ALTER TABLE `game`
MODIFY `matchId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `play`
--
ALTER TABLE `play`
MODIFY `idPlay` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `player`
--
ALTER TABLE `player`
MODIFY `idPlayer` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `search_game`
--
ALTER TABLE `search_game`
MODIFY `idSearch` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ask`
--
ALTER TABLE `ask`
ADD CONSTRAINT `FK_ASK_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`),
ADD CONSTRAINT `FK_ASK_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`);

--
-- Contraintes pour la table `board`
--
ALTER TABLE `board`
ADD CONSTRAINT `FK_BOARD_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_BOARD_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_BOARD_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `deck`
--
ALTER TABLE `deck`
ADD CONSTRAINT `FK_DECK_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_DECK_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_DECK_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
ADD CONSTRAINT `FK_GAME_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`);

--
-- Contraintes pour la table `hand`
--
ALTER TABLE `hand`
ADD CONSTRAINT `FK_HAND_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_HAND_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_HAND_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
ADD CONSTRAINT `FK_participe_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`),
ADD CONSTRAINT `FK_participe_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`);

--
-- Contraintes pour la table `play`
--
ALTER TABLE `play`
ADD CONSTRAINT `FK_PLAY_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_PLAY_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `search_game`
--
ALTER TABLE `search_game`
ADD CONSTRAINT `FK_SEARCH_GAME_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_SEARCH_GAME_idPlayer_PLAYER` FOREIGN KEY (`idPlayer2`) REFERENCES `player` (`idPlayer`);

--
-- Contraintes pour la table `tournament`
--
ALTER TABLE `tournament`
ADD CONSTRAINT `FK_TOURNAMENT_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
