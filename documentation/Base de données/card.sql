-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 13 Janvier 2018 à 15:48
-- Version du serveur :  5.5.58-0+deb8u1
-- Version de PHP :  5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `esgibattlecard`
--

-- --------------------------------------------------------

--
-- Structure de la table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `name` varchar(25) DEFAULT NULL,
`cardId` int(11) NOT NULL,
  `deck` int(11) DEFAULT NULL,
  `spell` int(11) DEFAULT NULL,
  `life` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `openCost` int(11) NOT NULL,
  `specialEffect` int(11) DEFAULT NULL,
  `spellEffect` int(11) DEFAULT NULL,
  `tinyPicture` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `card`
--

INSERT INTO `card` (`name`, `cardId`, `deck`, `spell`, `life`, `attack`, `openCost`, `specialEffect`, `spellEffect`, `tinyPicture`, `picture`) VALUES
('Mason', 1, 0, NULL, 4, 9, 1, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'HAC70KMG4KQ'),
('Galvin', 2, 0, NULL, 9, 0, 9, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'PSN81MIU5XV'),
('Philip', 3, 0, NULL, 6, 9, 3, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'BWR17VSY9XS'),
('Bell', 4, 0, NULL, 2, 9, 5, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'JYB46IRO6JK'),
('Eagan', 5, 0, NULL, 1, 4, 4, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'IUH84BJX1UA'),
('Stewart', 6, 0, NULL, 5, 8, 10, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'NFB30LDQ2MT'),
('Lane', 7, 0, NULL, 4, 3, 6, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'VYR75CEO1XL'),
('Germane', 8, 0, NULL, 6, 1, 8, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'PQX54KZY7RH'),
('Cara', 9, 0, NULL, 4, 7, 7, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'EAH60GZU2NY'),
('Rhona', 10, 0, NULL, 8, 3, 7, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'HOE30UPC7JZ'),
('Cruz', 11, 0, NULL, 2, 8, 7, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'HJX89XIO2EG'),
('Garrison', 12, 0, 2, NULL, NULL, 0, NULL, 2, 'images/cardImages/cartetest1.min.bmp', 'RIC27EKZ8ZU'),
('Victor', 13, 0, 1, NULL, NULL, 10, NULL, 3, 'images/cardImages/cartetest1.min.bmp', 'OEP93ZRA3VH'),
('Janna', 14, 0, 1, NULL, NULL, 8, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'PWB79BBH9JJ'),
('Dakota', 15, 0, 3, NULL, NULL, 1, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'OPZ23STQ7VZ'),
('Frances', 16, 0, NULL, 6, 1, 3, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'XUX50TGM5YY'),
('Jessica', 17, 0, NULL, 5, 7, 9, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'KGB73ZSA0YB'),
('Jason', 18, 0, NULL, 10, 7, 8, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'WZB14IRV0JW'),
('Rae', 19, 0, NULL, 1, 3, 0, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'SHK25KTK4VG'),
('Lars', 20, 0, NULL, 9, 9, 0, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'JUC27TEG8UW'),
('Carissa', 21, 0, NULL, 8, 0, 10, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'RYU28JKD1SA'),
('Regina', 22, 0, 1, NULL, NULL, 6, NULL, 3, 'images/cardImages/cartetest1.min.bmp', 'GLS15ZHL1PD'),
('Jakeem', 23, 0, 3, NULL, NULL, 3, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'CDO21KAS3DL'),
('Joan', 24, 0, 3, NULL, NULL, 4, NULL, 3, 'images/cardImages/cartetest1.min.bmp', 'SMM21XBP0ZT'),
('Jonas', 25, 0, 2, NULL, NULL, 10, NULL, 3, 'images/cardImages/cartetest1.min.bmp', 'JDO50HHZ7UM'),
('Devin', 26, 0, 0, NULL, NULL, 7, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'BXX10CKB1MF'),
('Noelle', 27, 0, 0, NULL, NULL, 6, NULL, 4, 'images/cardImages/cartetest1.min.bmp', 'GKX81LXY8BY'),
('Galena', 28, 0, NULL, 8, 7, 3, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'HUZ05VFU1VM'),
('Rinah', 29, 0, NULL, 1, 1, 0, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'ZFT10FFB1YI'),
('Imogene', 30, 0, NULL, 4, 0, 9, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'CHZ74AMN1RG'),
('Zane', 31, 1, NULL, 4, 6, 9, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'ZVO44FYZ3YM'),
('Zachery', 32, 1, NULL, 1, 8, 2, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'VQT47ATW1LA'),
('Alden', 33, 1, NULL, 9, 2, 2, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'BJZ20DIS1EV'),
('Fallon', 34, 1, NULL, 1, 10, 2, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'WOY18TNQ0WZ'),
('Athena', 35, 1, NULL, 4, 6, 9, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'YMX86UIQ2UT'),
('Heather', 36, 1, NULL, 2, 7, 6, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'MHM35KPD3SX'),
('Seth', 37, 1, NULL, 2, 7, 3, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'CSY13USQ6IE'),
('Deacon', 38, 1, NULL, 9, 7, 3, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'HCV93PZT3UC'),
('Cheyenne', 39, 1, NULL, 8, 7, 1, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'ECA26GQM5NB'),
('Lance', 40, 1, NULL, 2, 1, 1, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'XVN99SGN5AD'),
('Deanna', 41, 1, NULL, 1, 8, 9, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'EQD80VUS2DX'),
('Lillith', 42, 1, NULL, 9, 3, 10, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'UFC48DVD3WK'),
('Emma', 43, 1, NULL, 3, 2, 6, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'VAQ76GHF0EK'),
('Bryar', 44, 1, NULL, 1, 5, 3, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'JYU79FED6WO'),
('Lana', 45, 1, NULL, 10, 7, 3, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'DUF66EGG7RC'),
('Jamalia', 46, 1, NULL, 10, 2, 5, 1, NULL, 'images/cardImages/cartetest1.min.bmp', 'FSF09TYU9GR'),
('Raymond', 47, 1, NULL, 4, 8, 7, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'HEY23ROZ0NA'),
('Larissa', 48, 1, NULL, 1, 3, 4, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'WGS67GSY0US'),
('Avram', 49, 1, NULL, 6, 7, 6, 3, NULL, 'images/cardImages/cartetest1.min.bmp', 'UZS70DDI1BE'),
('Reagan', 50, 1, NULL, 8, 1, 0, 2, NULL, 'images/cardImages/cartetest1.min.bmp', 'WQN06HNZ9FS'),
('Nola', 51, 1, 1, NULL, NULL, 4, NULL, 2, 'images/cardImages/cartetest1.min.bmp', 'EYG40IXZ8KP'),
('Kenneth', 52, 1, 1, NULL, NULL, 8, NULL, 3, 'images/cardImages/cartetest1.min.bmp', 'UQD84YDE9GJ'),
('Dominic', 53, 1, 2, NULL, NULL, 2, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'RSF48AEB3AX'),
('Michelle', 54, 1, 3, NULL, NULL, 1, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'DSD49TAJ9US'),
('Jasper', 55, 1, 1, NULL, NULL, 0, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'VDD28YSY5IW'),
('Carter', 56, 1, 0, NULL, NULL, 2, NULL, 4, 'images/cardImages/cartetest1.min.bmp', 'ZQF09OHL6SR'),
('Warren', 57, 1, 2, NULL, NULL, 9, NULL, 2, 'images/cardImages/cartetest1.min.bmp', 'CNE94VXC1QR'),
('MacKensie', 58, 1, 0, NULL, NULL, 0, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'CKP35AXT9YJ'),
('Caldwell', 59, 1, 3, NULL, NULL, 9, NULL, 1, 'images/cardImages/cartetest1.min.bmp', 'PMV22KGA3YP'),
('Rhiannon', 60, 1, 1, NULL, NULL, 9, NULL, 5, 'images/cardImages/cartetest1.min.bmp', 'PKN13SWV5NI'),
('Cancre', 61, 0, NULL, 1, 2, 1, NULL, NULL, '', '');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `card`
--
ALTER TABLE `card`
 ADD PRIMARY KEY (`cardId`), ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `card`
--
ALTER TABLE `card`
MODIFY `cardId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
