-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 06 Janvier 2018 à 15:23
-- Version du serveur :  5.5.58-0+deb8u1
-- Version de PHP :  5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `esgibattlecard`
--

-- --------------------------------------------------------

--
-- Structure de la table `ask`
--

CREATE TABLE IF NOT EXISTS `ask` (
  `idPlayer` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `board`
--

CREATE TABLE IF NOT EXISTS `board` (
  `lifeModified` int(11) DEFAULT NULL,
  `attackModified` int(11) DEFAULT NULL,
  `sleep` tinyint(1) NOT NULL,
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `card`
--

CREATE TABLE IF NOT EXISTS `card` (
  `name` varchar(25) DEFAULT NULL,
`cardId` int(11) NOT NULL,
  `deck` int(11) DEFAULT NULL,
  `spell` int(11) DEFAULT NULL,
  `life` int(11) DEFAULT NULL,
  `attack` int(11) DEFAULT NULL,
  `openCost` int(11) NOT NULL,
  `specialEffect` int(11) DEFAULT NULL,
  `spellEffect` int(11) DEFAULT NULL,
  `tinyPicture` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `card`
--

INSERT INTO `card` (`name`, `cardId`, `deck`, `spell`, `life`, `attack`, `openCost`, `specialEffect`, `spellEffect`, `tinyPicture`, `picture`) VALUES
('Mason', 1, 0, NULL, 4, 9, 1, 1, NULL, 'NAK14QBA5LD', 'HAC70KMG4KQ'),
('Galvin', 2, 0, NULL, 9, 0, 9, 1, NULL, 'ZWB37ZLX7TX', 'PSN81MIU5XV'),
('Philip', 3, 0, NULL, 6, 9, 3, 3, NULL, 'CZY61EZY6CN', 'BWR17VSY9XS'),
('Bell', 4, 0, NULL, 2, 9, 5, 2, NULL, 'XFN11CNI0RQ', 'JYB46IRO6JK'),
('Eagan', 5, 0, NULL, 1, 4, 4, 2, NULL, 'AJF66OGP5EU', 'IUH84BJX1UA'),
('Stewart', 6, 0, NULL, 5, 8, 10, 1, NULL, 'RLH16KUU0NH', 'NFB30LDQ2MT'),
('Lane', 7, 0, NULL, 4, 3, 6, 3, NULL, 'RFO99BKF2VZ', 'VYR75CEO1XL'),
('Germane', 8, 0, NULL, 6, 1, 8, 3, NULL, 'VXV63VBC2LU', 'PQX54KZY7RH'),
('Cara', 9, 0, NULL, 4, 7, 7, 2, NULL, 'MHS20JKI3OG', 'EAH60GZU2NY'),
('Rhona', 10, 0, NULL, 8, 3, 7, 2, NULL, 'WPF64JRT0NU', 'HOE30UPC7JZ'),
('Cruz', 11, 0, NULL, 2, 8, 7, 1, NULL, 'IRG82QNY6JO', 'HJX89XIO2EG'),
('Garrison', 12, 0, 2, NULL, NULL, 0, NULL, 2, 'PRJ59AHI3KH', 'RIC27EKZ8ZU'),
('Victor', 13, 0, 1, NULL, NULL, 10, NULL, 3, 'KWC86UOF0XM', 'OEP93ZRA3VH'),
('Janna', 14, 0, 1, NULL, NULL, 8, NULL, 5, 'QHQ97WRG9RZ', 'PWB79BBH9JJ'),
('Dakota', 15, 0, 3, NULL, NULL, 1, NULL, 5, 'BCP51KEP9SP', 'OPZ23STQ7VZ'),
('Frances', 16, 0, NULL, 6, 1, 3, 1, NULL, 'FSI87XVP7BH', 'XUX50TGM5YY'),
('Jessica', 17, 0, NULL, 5, 7, 9, 2, NULL, 'FWT97LEV6AC', 'KGB73ZSA0YB'),
('Jason', 18, 0, NULL, 10, 7, 8, 1, NULL, 'CEF17HIS0VF', 'WZB14IRV0JW'),
('Rae', 19, 0, NULL, 1, 3, 0, 3, NULL, 'WFU92WOA7QA', 'SHK25KTK4VG'),
('Lars', 20, 0, NULL, 9, 9, 0, 2, NULL, 'ZMW93OMF1LK', 'JUC27TEG8UW'),
('Carissa', 21, 0, NULL, 8, 0, 10, 3, NULL, 'GJB97UGR9EU', 'RYU28JKD1SA'),
('Regina', 22, 0, 1, NULL, NULL, 6, NULL, 3, 'PIV33UKF3XN', 'GLS15ZHL1PD'),
('Jakeem', 23, 0, 3, NULL, NULL, 3, NULL, 5, 'RNC17JMW9CK', 'CDO21KAS3DL'),
('Joan', 24, 0, 3, NULL, NULL, 4, NULL, 3, 'RQU13VLG9MG', 'SMM21XBP0ZT'),
('Jonas', 25, 0, 2, NULL, NULL, 10, NULL, 3, 'RMK17FGQ9LC', 'JDO50HHZ7UM'),
('Devin', 26, 0, 0, NULL, NULL, 7, NULL, 5, 'KUR32FSL3ME', 'BXX10CKB1MF'),
('Noelle', 27, 0, 0, NULL, NULL, 6, NULL, 4, 'ONN04IYX2DJ', 'GKX81LXY8BY'),
('Galena', 28, 0, NULL, 8, 7, 3, 1, NULL, 'UWV59QEZ9KA', 'HUZ05VFU1VM'),
('Rinah', 29, 0, NULL, 1, 1, 0, 3, NULL, 'DDL67ZCN4IZ', 'ZFT10FFB1YI'),
('Imogene', 30, 0, NULL, 4, 0, 9, 3, NULL, 'PHA72INL4KV', 'CHZ74AMN1RG'),
('Zane', 31, 1, NULL, 4, 6, 9, 2, NULL, 'HNQ39VRD2AA', 'ZVO44FYZ3YM'),
('Zachery', 32, 1, NULL, 1, 8, 2, 1, NULL, 'HGN94BRQ9LQ', 'VQT47ATW1LA'),
('Alden', 33, 1, NULL, 9, 2, 2, 2, NULL, 'GQM81AJM7PW', 'BJZ20DIS1EV'),
('Fallon', 34, 1, NULL, 1, 10, 2, 2, NULL, 'UEW61NMP2BC', 'WOY18TNQ0WZ'),
('Athena', 35, 1, NULL, 4, 6, 9, 1, NULL, 'QJH54UVZ8YK', 'YMX86UIQ2UT'),
('Heather', 36, 1, NULL, 2, 7, 6, 1, NULL, 'MDI26BFS2OV', 'MHM35KPD3SX'),
('Seth', 37, 1, NULL, 2, 7, 3, 3, NULL, 'NZT41TNF3DZ', 'CSY13USQ6IE'),
('Deacon', 38, 1, NULL, 9, 7, 3, 3, NULL, 'TRF86GDM5NF', 'HCV93PZT3UC'),
('Cheyenne', 39, 1, NULL, 8, 7, 1, 2, NULL, 'CQI59ZAN4SX', 'ECA26GQM5NB'),
('Lance', 40, 1, NULL, 2, 1, 1, 2, NULL, 'AZL87AXY3VX', 'XVN99SGN5AD'),
('Deanna', 41, 1, NULL, 1, 8, 9, 2, NULL, 'PVS92QKG8IL', 'EQD80VUS2DX'),
('Lillith', 42, 1, NULL, 9, 3, 10, 1, NULL, 'YGF97QPP0RF', 'UFC48DVD3WK'),
('Emma', 43, 1, NULL, 3, 2, 6, 3, NULL, 'FHP10QJI5KA', 'VAQ76GHF0EK'),
('Bryar', 44, 1, NULL, 1, 5, 3, 2, NULL, 'TGD42KCR2IV', 'JYU79FED6WO'),
('Lana', 45, 1, NULL, 10, 7, 3, 1, NULL, 'BPR48MXM8YQ', 'DUF66EGG7RC'),
('Jamalia', 46, 1, NULL, 10, 2, 5, 1, NULL, 'XQT26FSO3QY', 'FSF09TYU9GR'),
('Raymond', 47, 1, NULL, 4, 8, 7, 3, NULL, 'ZAH05HKD8YM', 'HEY23ROZ0NA'),
('Larissa', 48, 1, NULL, 1, 3, 4, 2, NULL, 'PXE95ZQS6JY', 'WGS67GSY0US'),
('Avram', 49, 1, NULL, 6, 7, 6, 3, NULL, 'HSA53HTM5EU', 'UZS70DDI1BE'),
('Reagan', 50, 1, NULL, 8, 1, 0, 2, NULL, 'ERS84CFG2KJ', 'WQN06HNZ9FS'),
('Nola', 51, 1, 1, NULL, NULL, 4, NULL, 2, 'DIP32IIX5QV', 'EYG40IXZ8KP'),
('Kenneth', 52, 1, 1, NULL, NULL, 8, NULL, 3, 'QKA99BZJ5WT', 'UQD84YDE9GJ'),
('Dominic', 53, 1, 2, NULL, NULL, 2, NULL, 5, 'ZTI80FBG7VM', 'RSF48AEB3AX'),
('Michelle', 54, 1, 3, NULL, NULL, 1, NULL, 5, 'RKG85IGV0CK', 'DSD49TAJ9US'),
('Jasper', 55, 1, 1, NULL, NULL, 0, NULL, 5, 'NQU75AXC3BM', 'VDD28YSY5IW'),
('Carter', 56, 1, 0, NULL, NULL, 2, NULL, 4, 'DFQ34YAT2GY', 'ZQF09OHL6SR'),
('Warren', 57, 1, 2, NULL, NULL, 9, NULL, 2, 'XOD69HYC9QU', 'CNE94VXC1QR'),
('MacKensie', 58, 1, 0, NULL, NULL, 0, NULL, 5, 'WYK00LVH8UV', 'CKP35AXT9YJ'),
('Caldwell', 59, 1, 3, NULL, NULL, 9, NULL, 1, 'DVG44FDO0JG', 'PMV22KGA3YP'),
('Rhiannon', 60, 1, 1, NULL, NULL, 9, NULL, 5, 'KDV02OEQ0SU', 'PKN13SWV5NI');

-- --------------------------------------------------------

--
-- Structure de la table `deck`
--

CREATE TABLE IF NOT EXISTS `deck` (
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `deck`
--

INSERT INTO `deck` (`cardId`, `matchId`, `idPlayer`) VALUES
(25, 43, 42),
(31, 43, 32),
(1, 48, 40),
(3, 48, 40),
(4, 48, 40),
(5, 48, 40),
(6, 48, 40),
(7, 48, 40),
(8, 48, 40),
(9, 48, 40),
(10, 48, 40),
(12, 48, 40),
(13, 48, 40),
(14, 48, 40),
(15, 48, 40),
(16, 48, 40),
(17, 48, 40),
(18, 48, 40),
(19, 48, 40),
(20, 48, 40),
(22, 48, 40),
(23, 48, 40),
(24, 48, 40),
(25, 48, 40),
(26, 48, 40),
(27, 48, 40),
(28, 48, 40),
(29, 48, 40),
(30, 48, 40),
(31, 48, 30),
(32, 48, 30),
(33, 48, 30),
(34, 48, 30),
(35, 48, 30),
(36, 48, 30),
(37, 48, 30),
(38, 48, 30),
(39, 48, 30),
(40, 48, 30),
(41, 48, 30),
(42, 48, 30),
(43, 48, 30),
(44, 48, 30),
(45, 48, 30),
(46, 48, 30),
(48, 48, 30),
(49, 48, 30),
(51, 48, 30),
(52, 48, 30),
(53, 48, 30),
(54, 48, 30),
(55, 48, 30),
(57, 48, 30),
(58, 48, 30),
(59, 48, 30),
(60, 48, 30),
(1, 62, 43),
(2, 62, 43),
(3, 62, 43),
(4, 62, 43),
(5, 62, 43),
(6, 62, 43),
(7, 62, 43),
(8, 62, 43),
(10, 62, 43),
(11, 62, 43),
(12, 62, 43),
(13, 62, 43),
(14, 62, 43),
(15, 62, 43),
(16, 62, 43),
(17, 62, 43),
(19, 62, 43),
(21, 62, 43),
(22, 62, 43),
(23, 62, 43),
(24, 62, 43),
(25, 62, 43),
(26, 62, 43),
(27, 62, 43),
(28, 62, 43),
(29, 62, 43),
(30, 62, 13),
(30, 62, 43),
(31, 62, 38),
(33, 62, 38),
(34, 62, 38),
(35, 62, 38),
(36, 62, 38),
(37, 62, 38),
(38, 62, 38),
(40, 62, 38),
(41, 62, 38),
(42, 62, 38),
(43, 62, 38),
(44, 62, 38),
(45, 62, 38),
(46, 62, 38),
(47, 62, 38),
(48, 62, 38),
(49, 62, 38),
(50, 62, 38),
(51, 62, 38),
(52, 62, 38),
(53, 62, 38),
(54, 62, 38),
(56, 62, 38),
(57, 62, 38),
(58, 62, 38),
(58, 62, 43),
(59, 62, 38),
(60, 62, 38),
(1, 67, 45),
(3, 67, 45),
(4, 67, 45),
(5, 67, 45),
(6, 67, 45),
(7, 67, 45),
(8, 67, 45),
(9, 67, 45),
(10, 67, 45),
(11, 67, 45),
(12, 67, 45),
(13, 67, 45),
(14, 67, 45),
(15, 67, 45),
(16, 67, 45),
(17, 67, 45),
(18, 67, 45),
(19, 67, 45),
(20, 67, 45),
(21, 67, 45),
(22, 67, 45),
(23, 67, 45),
(26, 67, 45),
(27, 67, 45),
(28, 67, 45),
(29, 67, 45),
(30, 67, 45),
(31, 67, 25),
(32, 67, 25),
(33, 67, 25),
(34, 67, 25),
(35, 67, 25),
(36, 67, 25),
(37, 67, 25),
(38, 67, 25),
(41, 67, 25),
(42, 67, 25),
(43, 67, 25),
(44, 67, 25),
(45, 67, 25),
(46, 67, 25),
(47, 67, 25),
(48, 67, 25),
(49, 67, 25),
(50, 67, 25),
(51, 67, 25),
(52, 67, 25),
(53, 67, 25),
(54, 67, 25),
(56, 67, 25),
(57, 67, 25),
(58, 67, 25),
(59, 67, 25),
(60, 67, 25),
(1, 68, 44),
(2, 68, 44),
(3, 68, 44),
(4, 68, 44),
(5, 68, 44),
(6, 68, 44),
(7, 68, 44),
(9, 68, 44),
(10, 68, 44),
(11, 68, 44),
(12, 68, 44),
(13, 68, 44),
(14, 68, 44),
(16, 68, 44),
(17, 68, 44),
(18, 68, 44),
(19, 68, 44),
(20, 68, 44),
(21, 68, 44),
(22, 68, 44),
(23, 68, 44),
(24, 68, 44),
(25, 68, 44),
(26, 68, 44),
(27, 68, 44),
(28, 68, 44),
(30, 68, 44),
(31, 68, 34),
(32, 68, 34),
(33, 68, 34),
(34, 68, 34),
(35, 68, 34),
(36, 68, 34),
(37, 68, 34),
(38, 68, 34),
(39, 68, 34),
(41, 68, 34),
(42, 68, 34),
(43, 68, 34),
(45, 68, 34),
(46, 68, 34),
(47, 68, 34),
(48, 68, 34),
(50, 68, 34),
(51, 68, 34),
(52, 68, 34),
(53, 68, 34),
(54, 68, 34),
(55, 68, 34),
(56, 68, 34),
(57, 68, 34),
(58, 68, 34),
(59, 68, 34),
(60, 68, 34),
(1, 69, 1),
(2, 69, 1),
(3, 69, 1),
(6, 69, 1),
(7, 69, 1),
(8, 69, 1),
(9, 69, 1),
(10, 69, 1),
(11, 69, 1),
(12, 69, 1),
(13, 69, 1),
(14, 69, 1),
(15, 69, 1),
(16, 69, 1),
(17, 69, 1),
(18, 69, 1),
(19, 69, 1),
(21, 69, 1),
(22, 69, 1),
(23, 69, 1),
(24, 69, 1),
(25, 69, 1),
(26, 69, 1),
(27, 69, 1),
(28, 69, 1),
(29, 69, 1),
(30, 69, 1),
(31, 69, 37),
(32, 69, 37),
(33, 69, 37),
(34, 69, 37),
(35, 69, 37),
(36, 69, 37),
(37, 69, 37),
(38, 69, 37),
(39, 69, 37),
(40, 69, 37),
(41, 69, 37),
(43, 69, 37),
(44, 69, 37),
(45, 69, 37),
(46, 69, 37),
(47, 69, 37),
(48, 69, 37),
(49, 69, 37),
(50, 69, 37),
(51, 69, 37),
(52, 69, 37),
(53, 69, 37),
(54, 69, 37),
(55, 69, 37),
(57, 69, 37),
(58, 69, 37),
(60, 69, 37);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
`matchId` int(11) NOT NULL,
  `turn` int(11) DEFAULT NULL,
  `firstPlayer` int(11) DEFAULT NULL,
  `round` int(11) NOT NULL DEFAULT '1',
  `save` tinyint(1) NOT NULL DEFAULT '0',
  `winnerId` int(11) DEFAULT NULL,
  `looserId` int(11) DEFAULT NULL,
  `tournamentId` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `game`
--

INSERT INTO `game` (`matchId`, `turn`, `firstPlayer`, `round`, `save`, `winnerId`, `looserId`, `tournamentId`) VALUES
(1, NULL, NULL, 8, 0, NULL, NULL, NULL),
(2, NULL, NULL, 4, 1, NULL, NULL, NULL),
(3, NULL, NULL, 6, 1, NULL, NULL, NULL),
(4, NULL, NULL, 2, 0, NULL, NULL, NULL),
(5, NULL, NULL, 7, 1, NULL, NULL, NULL),
(6, NULL, NULL, 7, 0, NULL, NULL, NULL),
(7, NULL, NULL, 7, 0, NULL, NULL, NULL),
(8, NULL, NULL, 2, 1, NULL, NULL, NULL),
(9, NULL, NULL, 2, 0, NULL, NULL, NULL),
(10, NULL, NULL, 8, 1, NULL, NULL, NULL),
(11, NULL, NULL, 8, 0, NULL, NULL, NULL),
(12, NULL, NULL, 9, 0, NULL, NULL, NULL),
(13, NULL, NULL, 8, 1, NULL, NULL, NULL),
(14, NULL, NULL, 10, 0, NULL, NULL, NULL),
(15, NULL, NULL, 3, 0, NULL, NULL, NULL),
(16, NULL, NULL, 5, 0, NULL, NULL, NULL),
(17, NULL, NULL, 3, 1, NULL, NULL, NULL),
(18, NULL, NULL, 10, 1, NULL, NULL, NULL),
(19, NULL, NULL, 1, 1, NULL, NULL, NULL),
(20, NULL, NULL, 3, 0, NULL, NULL, NULL),
(22, 14, 14, 1, 0, NULL, NULL, NULL),
(23, 19, 19, 1, 0, NULL, NULL, NULL),
(24, 14, 14, 1, 0, NULL, NULL, NULL),
(25, 16, 16, 1, 0, NULL, NULL, NULL),
(26, 20, 20, 1, 0, NULL, NULL, NULL),
(27, 15, 15, 1, 0, NULL, NULL, NULL),
(28, 13, 13, 1, 0, NULL, NULL, NULL),
(29, 15, 15, 1, 0, NULL, NULL, NULL),
(30, 19, 19, 1, 0, NULL, NULL, NULL),
(31, 17, 17, 1, 0, NULL, NULL, NULL),
(32, 18, 18, 1, 0, NULL, NULL, NULL),
(33, 15, 15, 1, 0, NULL, NULL, NULL),
(34, 15, 15, 1, 0, NULL, NULL, NULL),
(35, 19, 19, 1, 0, NULL, NULL, NULL),
(36, 15, 15, 1, 0, NULL, NULL, NULL),
(37, 16, 16, 1, 0, NULL, NULL, NULL),
(38, 15, 15, 1, 0, NULL, NULL, NULL),
(39, 12, 12, 1, 0, NULL, NULL, NULL),
(40, 9, 9, 1, 0, NULL, NULL, NULL),
(41, 5, 5, 1, 0, NULL, NULL, NULL),
(42, 26, 26, 1, 0, NULL, NULL, NULL),
(43, 32, 32, 1, 0, NULL, NULL, NULL),
(44, 4, 4, 1, 0, NULL, NULL, NULL),
(45, 12, 12, 1, 0, NULL, NULL, NULL),
(46, 24, 24, 1, 0, NULL, NULL, NULL),
(47, 12, 12, 1, 0, NULL, NULL, NULL),
(48, 40, 40, 1, 0, NULL, NULL, NULL),
(49, 14, 14, 1, 0, NULL, NULL, NULL),
(50, 28, 28, 1, 0, NULL, NULL, NULL),
(51, 38, 38, 1, 0, NULL, NULL, NULL),
(52, 22, 22, 1, 0, NULL, NULL, NULL),
(53, 25, 25, 1, 0, NULL, NULL, NULL),
(54, 18, 18, 1, 0, NULL, NULL, NULL),
(55, 13, 13, 1, 0, NULL, NULL, NULL),
(56, 14, 14, 1, 0, NULL, NULL, NULL),
(57, 33, 33, 1, 0, NULL, NULL, NULL),
(58, 31, 31, 1, 0, NULL, NULL, NULL),
(59, 42, 42, 1, 0, NULL, NULL, NULL),
(60, 11, 11, 1, 0, NULL, NULL, NULL),
(61, 8, 8, 1, 0, NULL, NULL, NULL),
(62, 43, 43, 1, 0, NULL, NULL, NULL),
(63, 8, 8, 1, 0, NULL, NULL, NULL),
(64, 32, 32, 1, 0, NULL, NULL, NULL),
(65, 28, 28, 1, 0, NULL, NULL, NULL),
(66, 21, 21, 1, 0, NULL, NULL, NULL),
(67, 45, 45, 1, 0, NULL, NULL, NULL),
(68, 34, 34, 1, 0, NULL, NULL, NULL),
(69, 37, 37, 1, 0, NULL, NULL, NULL),
(70, 40, 40, 1, 0, NULL, NULL, NULL),
(71, 43, 43, 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `hand`
--

CREATE TABLE IF NOT EXISTS `hand` (
  `cardId` int(11) NOT NULL,
  `matchId` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL,
  `try` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `hand`
--

INSERT INTO `hand` (`cardId`, `matchId`, `idPlayer`, `try`) VALUES
(1, 43, 42, 0),
(1, 60, 2, 0),
(1, 62, 13, 0),
(2, 48, 40, 0),
(2, 62, 13, 0),
(2, 67, 45, 0),
(4, 69, 1, 0),
(5, 69, 1, 0),
(8, 68, 44, 0),
(9, 62, 43, 0),
(11, 48, 40, 0),
(15, 43, 42, 0),
(15, 68, 44, 0),
(18, 62, 43, 0),
(20, 62, 43, 0),
(20, 69, 1, 0),
(21, 48, 40, 0),
(24, 67, 45, 0),
(25, 67, 45, 0),
(27, 60, 2, 0),
(29, 62, 13, 0),
(29, 68, 44, 0),
(30, 43, 42, 0),
(30, 60, 2, 0),
(31, 60, 11, 0),
(32, 62, 38, 0),
(33, 62, 43, 0),
(39, 62, 38, 0),
(39, 67, 25, 0),
(40, 67, 25, 0),
(40, 68, 34, 0),
(42, 69, 37, 0),
(44, 68, 34, 0),
(47, 48, 30, 0),
(49, 68, 34, 0),
(50, 48, 30, 0),
(54, 43, 32, 0),
(54, 62, 43, 0),
(55, 62, 38, 0),
(55, 67, 25, 0),
(56, 48, 30, 0),
(56, 69, 37, 0),
(59, 43, 32, 0),
(59, 69, 37, 0),
(60, 43, 32, 0),
(60, 60, 11, 0),
(60, 62, 43, 0);

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

CREATE TABLE IF NOT EXISTS `participe` (
  `satus` int(11) NOT NULL DEFAULT '0',
  `turnOut` int(11) DEFAULT NULL,
  `idPlayer` int(11) NOT NULL,
  `tournamentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `play`
--

CREATE TABLE IF NOT EXISTS `play` (
`idPlay` int(11) NOT NULL,
  `playerLife` int(11) NOT NULL DEFAULT '35',
  `idPlayer` int(11) NOT NULL,
  `matchId` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `play`
--

INSERT INTO `play` (`idPlay`, `playerLife`, `idPlayer`, `matchId`) VALUES
(71, 35, 45, 67),
(72, 35, 25, 67),
(73, 35, 44, 68),
(74, 35, 34, 68),
(75, 35, 1, 69),
(76, 35, 37, 69),
(77, 35, 40, 48),
(78, 35, 30, 48),
(79, 35, 43, 62),
(80, 35, 38, 62);

-- --------------------------------------------------------

--
-- Structure de la table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
`idPlayer` int(11) NOT NULL,
  `nickname` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `win` int(11) NOT NULL DEFAULT '0',
  `loose` int(11) NOT NULL DEFAULT '0',
  `currentTournament` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `player`
--

INSERT INTO `player` (`idPlayer`, `nickname`, `email`, `password`, `token`, `avatar`, `win`, `loose`, `currentTournament`) VALUES
(1, 'IA', 'libart@libart.xyz', 'esgi2017', NULL, NULL, 100, 2, 0),
(2, 'Owen', 'lobortis@vel.co.uk', 'ADP41VBA3MK', NULL, NULL, 24, 68, 0),
(3, 'Cheryl', 'lorem@neque.org', 'ABX87GDI1GT', NULL, NULL, 60, 5, 0),
(4, 'Jin', 'elementum.purus.accumsan@nisl.co.uk', 'BAX75SLU9NT', NULL, NULL, 85, 89, 0),
(5, 'Kamal', 'nulla.Integer@accumsansed.net', 'EJB31XFH7LM', NULL, NULL, 11, 31, 0),
(6, 'Ashely', 'orci@porttitor.org', 'YBW20GCK9GK', NULL, NULL, 17, 5, 0),
(7, 'Gloria', 'facilisis@quamPellentesquehabitant.net', 'RMS73XSS1MA', NULL, NULL, 38, 36, 0),
(8, 'Isaac', 'eu.neque.pellentesque@non.org', 'WYU74NCT8SG', NULL, NULL, 21, 92, 0),
(9, 'Donna', 'eu.eleifend.nec@nuncacmattis.com', 'XPM20YQY2VI', NULL, NULL, 47, 41, 0),
(10, 'Abbot', 'sem@netus.ca', 'YKD58ODE9TI', NULL, NULL, 95, 77, 0),
(11, 'Jolie', 'aliquam.eu@interdumSedauctor.net', 'OHH69MWH5UR', NULL, NULL, 85, 62, 0),
(12, 'Sylvia', 'Fusce.feugiat@volutpat.edu', 'IIJ57MER9XW', NULL, NULL, 86, 54, 0),
(13, 'Vance', 'Proin@odio.co.uk', 'UIG15GDR1GC', NULL, NULL, 48, 73, 0),
(14, 'TaShya', 'Cras.dictum@placeratCrasdictum.edu', 'GPH72DPG2CQ', NULL, NULL, 75, 21, 0),
(15, 'Mia', 'nisl.Quisque.fringilla@magnaSuspendissetristique.c', 'TGJ97XYB1YY', NULL, NULL, 9, 54, 0),
(16, 'Karly', 'lacus.Aliquam.rutrum@fames.com', 'KYY69LYS5GZ', NULL, NULL, 67, 77, 0),
(17, 'Cain', 'tristique@semperauctor.org', 'XBA09NNK4LW', NULL, NULL, 88, 99, 0),
(18, 'Mufutau', 'consequat.lectus.sit@semperegestasurna.ca', 'HSZ95AHC1IN', NULL, NULL, 37, 61, 0),
(19, 'Gavin', 'in@vulputateeu.org', 'PCK11KCH5WK', NULL, NULL, 57, 7, 0),
(20, 'Claudia', 'a.nunc.In@Namac.org', 'SOB37PBO6VG', NULL, NULL, 52, 3, 0),
(21, 'Gemma', 'Suspendisse.sagittis.Nullam@maurisSuspendissealiqu', 'QLK71JFM8BM', NULL, NULL, 27, 14, 0),
(22, 'Clarke', 'turpis@egetvolutpatornare.ca', 'GPO13GSV7QZ', NULL, NULL, 45, 77, 0),
(23, 'Channing', 'nulla@utnisi.net', 'PEI12EXG7PT', NULL, NULL, 23, 57, 0),
(24, 'Hunter', 'nisl.arcu@purusaccumsaninterdum.edu', 'RYU66SUU9VN', NULL, NULL, 6, 23, 0),
(25, 'Ferdinand', 'Fusce.mollis.Duis@fringillaest.net', 'OOA88TMG3JT', NULL, NULL, 39, 88, 0),
(26, 'Quynn', 'semper.rutrum@mattis.net', 'BEY66UUX3JT', NULL, NULL, 49, 6, 0),
(27, 'Brenda', 'venenatis@Maurismagna.co.uk', 'ZLE78LWW2QC', NULL, NULL, 100, 72, 0),
(28, 'Vanna', 'sed.hendrerit@pedeSuspendissedui.org', 'TXY63LFB7LC', NULL, NULL, 7, 78, 0),
(29, 'Alec', 'dapibus@dolorelit.net', 'NLF70LMS7AD', NULL, NULL, 45, 61, 0),
(30, 'Davis', 'ac.nulla.In@tempor.ca', 'OON97LHZ9CN', NULL, NULL, 58, 85, 0),
(31, 'Byron', 'tempus.scelerisque.lorem@velvenenatis.ca', 'SKE26XOA8XT', NULL, NULL, 99, 95, 0),
(32, 'Athena', 'pretium.et.rutrum@Crasdictumultricies.co.uk', 'TLV38UAM6GZ', NULL, NULL, 39, 69, 0),
(33, 'Lara', 'eu@Quisque.org', 'SJR22PVW1PD', NULL, NULL, 94, 46, 0),
(34, 'Akeem', 'Nunc@egestasa.net', 'XYV45WOL1FB', NULL, NULL, 21, 31, 0),
(35, 'Brendan', 'quis.arcu.vel@posuerecubiliaCurae.org', 'AHT38CEG3KL', NULL, NULL, 44, 94, 0),
(36, 'Chiquita', 'nunc.sit.amet@suscipit.edu', 'TRE70OJU3EU', NULL, NULL, 61, 78, 0),
(37, 'Jerry', 'dis@gravidasitamet.net', 'YLA39RKH3RZ', NULL, NULL, 47, 61, 0),
(38, 'Tallulah', 'Sed.eu.nibh@facilisis.net', 'JVD45MNB1PB', NULL, NULL, 41, 92, 0),
(39, 'Idona', 'lorem@arcuSedet.co.uk', 'ICK61XGX1DP', NULL, NULL, 44, 60, 0),
(40, 'Ignatius', 'nunc.id@tempusmauriserat.com', 'TFK27WII1FL', NULL, NULL, 31, 20, 0),
(41, 'Mikayla', 'arcu.ac@estmauris.net', 'DBA98UVQ8KW', NULL, NULL, 71, 92, 0),
(42, 'Regan', 'Vivamus@molestiepharetra.co.uk', 'OXR64SOA2GK', NULL, NULL, 52, 15, 0),
(43, 'Uriel', 'feugiat@aliquetnec.ca', 'GWY52VNW2LI', NULL, NULL, 61, 85, 0),
(44, 'Ivor', 'ante.ipsum@magna.edu', 'SHQ10LPE8DL', NULL, NULL, 18, 23, 0),
(45, 'Sierra', 'Nam.porttitor@Maurisquis.com', 'ZSK48VVD1NG', NULL, NULL, 83, 8, 0),
(46, 'Angelica', 'suscipit.nonummy@amet.org', 'WPF25SMB8VZ', NULL, NULL, 41, 49, 0);

-- --------------------------------------------------------

--
-- Structure de la table `search_game`
--

CREATE TABLE IF NOT EXISTS `search_game` (
`idSearch` int(11) NOT NULL,
  `idPlayer` int(11) NOT NULL,
  `idPlayer2` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tournament`
--

CREATE TABLE IF NOT EXISTS `tournament` (
  `tournamentId` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `playerNumber` int(11) DEFAULT NULL,
  `idPlayer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `ask`
--
ALTER TABLE `ask`
 ADD PRIMARY KEY (`idPlayer`,`tournamentId`), ADD KEY `FK_ASK_tournamentId` (`tournamentId`);

--
-- Index pour la table `board`
--
ALTER TABLE `board`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_BOARD_matchId` (`matchId`), ADD KEY `FK_BOARD_idPlayer` (`idPlayer`);

--
-- Index pour la table `card`
--
ALTER TABLE `card`
 ADD PRIMARY KEY (`cardId`), ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `deck`
--
ALTER TABLE `deck`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_DECK_matchId` (`matchId`), ADD KEY `FK_DECK_idPlayer` (`idPlayer`);

--
-- Index pour la table `game`
--
ALTER TABLE `game`
 ADD PRIMARY KEY (`matchId`), ADD KEY `FK_GAME_tournamentId` (`tournamentId`);

--
-- Index pour la table `hand`
--
ALTER TABLE `hand`
 ADD PRIMARY KEY (`cardId`,`matchId`,`idPlayer`), ADD KEY `FK_HAND_matchId` (`matchId`), ADD KEY `FK_HAND_idPlayer` (`idPlayer`);

--
-- Index pour la table `participe`
--
ALTER TABLE `participe`
 ADD PRIMARY KEY (`idPlayer`,`tournamentId`), ADD KEY `FK_participe_tournamentId` (`tournamentId`);

--
-- Index pour la table `play`
--
ALTER TABLE `play`
 ADD PRIMARY KEY (`idPlay`), ADD KEY `FK_PLAY_idPlayer` (`idPlayer`), ADD KEY `FK_PLAY_matchId` (`matchId`);

--
-- Index pour la table `player`
--
ALTER TABLE `player`
 ADD PRIMARY KEY (`idPlayer`), ADD UNIQUE KEY `nickname` (`nickname`,`email`,`token`);

--
-- Index pour la table `search_game`
--
ALTER TABLE `search_game`
 ADD PRIMARY KEY (`idSearch`), ADD KEY `FK_SEARCH_GAME_idPlayer_PLAYER` (`idPlayer2`), ADD KEY `FK_SEARCH_GAME_idPlayer` (`idPlayer`);

--
-- Index pour la table `tournament`
--
ALTER TABLE `tournament`
 ADD PRIMARY KEY (`tournamentId`), ADD KEY `FK_TOURNAMENT_idPlayer` (`idPlayer`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `card`
--
ALTER TABLE `card`
MODIFY `cardId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `game`
--
ALTER TABLE `game`
MODIFY `matchId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT pour la table `play`
--
ALTER TABLE `play`
MODIFY `idPlay` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT pour la table `player`
--
ALTER TABLE `player`
MODIFY `idPlayer` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `search_game`
--
ALTER TABLE `search_game`
MODIFY `idSearch` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ask`
--
ALTER TABLE `ask`
ADD CONSTRAINT `FK_ASK_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_ASK_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`);

--
-- Contraintes pour la table `board`
--
ALTER TABLE `board`
ADD CONSTRAINT `FK_BOARD_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_BOARD_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_BOARD_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `deck`
--
ALTER TABLE `deck`
ADD CONSTRAINT `FK_DECK_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_DECK_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_DECK_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
ADD CONSTRAINT `FK_GAME_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`);

--
-- Contraintes pour la table `hand`
--
ALTER TABLE `hand`
ADD CONSTRAINT `FK_HAND_cardId` FOREIGN KEY (`cardId`) REFERENCES `card` (`cardId`),
ADD CONSTRAINT `FK_HAND_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_HAND_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
ADD CONSTRAINT `FK_participe_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_participe_tournamentId` FOREIGN KEY (`tournamentId`) REFERENCES `tournament` (`tournamentId`);

--
-- Contraintes pour la table `play`
--
ALTER TABLE `play`
ADD CONSTRAINT `FK_PLAY_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_PLAY_matchId` FOREIGN KEY (`matchId`) REFERENCES `game` (`matchId`);

--
-- Contraintes pour la table `search_game`
--
ALTER TABLE `search_game`
ADD CONSTRAINT `FK_SEARCH_GAME_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`),
ADD CONSTRAINT `FK_SEARCH_GAME_idPlayer_PLAYER` FOREIGN KEY (`idPlayer2`) REFERENCES `player` (`idPlayer`);

--
-- Contraintes pour la table `tournament`
--
ALTER TABLE `tournament`
ADD CONSTRAINT `FK_TOURNAMENT_idPlayer` FOREIGN KEY (`idPlayer`) REFERENCES `player` (`idPlayer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
